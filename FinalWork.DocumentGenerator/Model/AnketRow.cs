﻿using FinalWork.DocumentGenerator.Attributes;

namespace FinalWork.DocumentGenerator.Model
{
    public class AnketRow
    {
        /// <summary>
        ///     Имя
        /// </summary>
        [Field("FirstName")]
        public string FirstName { get; set; }

        /// <summary>
        ///     Отчество
        /// </summary>
        [Field("MiddleName")]
        public string MiddleName { get; set; }

        /// <summary>
        ///     Фамилия
        /// </summary>
        [Field("LastName")]
        public string LastName { get; set; }

        /// <summary>
        ///     Группа
        /// </summary>
        [Field("Group")]
        public string Group { get; set; }

        /// <summary>
        ///     Прошлое образование
        /// </summary>
        [Field("PastEducation")]
        public string PastEducation { get; set; }

        /// <summary>
        ///     Документ о прошлом образовании
        /// </summary>
        [Field("PastEducationDoc")]
        public string PastEducationDocument { get; set; }

        /// <summary>
        ///     Дата рождения
        /// </summary>
        [Field("BirthDay")]
        public string BirthDay { get; set; }

        /// <summary>
        ///     Дата рождения
        /// </summary>
        [Field("PastEducationEnd")]
        public string PastEducationYear { get; set; }

        /// <summary>
        ///     Платн/бюдж
        /// </summary>
        [Field("Payer")]
        public string EducationPay { get; set; }

        /// <summary>
        ///     Наименование учебного заведения
        /// </summary>
        [Field("PastEducationName")]
        public string PastEducationOrganization { get; set; }
    }
}

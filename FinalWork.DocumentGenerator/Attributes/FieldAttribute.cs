﻿using System;

namespace FinalWork.DocumentGenerator.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    class FieldAttribute
        : Attribute
    {
        public string Tag { get; }

        public FieldAttribute(string tag)
        {
            Tag = tag;
        }
    }
}

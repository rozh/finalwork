using TemplateEngine.Docx;

namespace FinalWork.DocumentGenerator.Interfaces
{
    public interface IDocument
    {
        Content Generate();
    }
}
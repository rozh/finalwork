﻿using System.Collections.Generic;
using FinalWork.DocumentGenerator.Document.Base;
using FinalWork.DocumentGenerator.Interfaces;
using FinalWork.DocumentGenerator.Model;
using FinalWork.Model;
using TemplateEngine.Docx;

namespace FinalWork.DocumentGenerator.Document
{
    public class Anket 
        : DocumentBase, IDocument
    {
        public Anket() : base(Templates.anketa_template)
        {
            AnketRows = new List<AnketRow>();
        }

        public Anket(IEnumerable<Student> students) : this()
        {
            foreach (var student in students)
            {
                AnketRows.Add(new AnketRow()
                {
                    FirstName = student.FirstName,
                    LastName = student.LastName,
                    MiddleName = student.MiddleName,
                    Group = student.Group,
                });
            }
        }


        public List<AnketRow> AnketRows { get; set; }

        public override Content Generate()
        {
            var table = new TableContent("Anketa");

            foreach (var anketRow in AnketRows)
            {
                table.AddRow(GetFields(anketRow));
            }

            return new Content(
                table
            );
        }
    }
}

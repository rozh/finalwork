using System.Collections.Generic;
using System.IO;
using System.Linq;
using FinalWork.DocumentGenerator.Attributes;
using TemplateEngine.Docx;

namespace FinalWork.DocumentGenerator.Document.Base
{
    public abstract class DocumentBase
    {
        public byte[] Template { get; }

        public DocumentBase(byte[] template)
        {
            Template = template;
        }

        public abstract Content Generate();

        protected IContentItem[] GetFields(object data)
        {
            var props = data.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(FieldAttribute), true).Any());
            var result = new List<IContentItem>();

            foreach (var prop in props)
            {
                var attr = (FieldAttribute)prop.GetCustomAttributes(typeof(FieldAttribute), true).First();
                result.Add(new FieldContent(attr.Tag, prop.GetValue(data)?.ToString() ?? string.Empty));
            }

            return result.ToArray();
        }

        public void Save(string fileName)
        {
            File.WriteAllBytes(fileName, Template);

            var valuesToFill = Generate();
            using (var outputDocument = new TemplateProcessor(fileName)
                .SetRemoveContentControls(true))
            {
                outputDocument.FillContent(valuesToFill);
                outputDocument.SaveChanges();
            }
        }
    }
}
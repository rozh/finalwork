﻿# FinalWork

Эта программулина нужна для автоматизации проведения защит студентов.

## Начало работы

### Требования

Для работы программы требуется

* MySQL 5.7 
* .Net Framework 4.5.2

### Установка

Инсталлятора на данный момент нет. Поэтому можно скачать стабильную версию из загрузок или собрать из исходного кода.

Для работы потребуется настроить сервер БД.

### СУБД

Используемая СУБД: [MySQL 5.7](https://bitbucket.org/rozh/finalwork/downloads/mysql_5.7.9_x64.exe)

В состав сборки входит:

* Сама СУБД MySQL 5.7.9
* Конфигурация `my.ini`
* Скрипт инициализации структуры хранения данных (*init_data_dir.bat*)
* Скрипты запуска (*start_mysqld.bat*) и остановки (*stop_mysqld.bat*)
* Скрипты установки (*service_install.bat*) и удаления службы (*service_uninstall.bat*)

#### Установка

1. Скачать и распаковать СУБД [MySQL 5.7](https://bitbucket.org/rozh/finalwork/downloads/mysql_5.7.9_x64.exe)
2. В директории СУБД запустить *init_data_dir.bat*
3. Запустить *start_mysqld.bat* для запуска сервера или *service_install.bat* для установки службы и запуска сервера. После установки как службы MySQL будет автоматически запускаться при старте системы.

#### Подключение к MySQL

После запуска *init_data_dir.bat* и старта СУБД подключиться можно с пользованием логина **root** без пароля.

За подключение к СУБД в программе отвечает строка файла *FinalWork.exe.config*:

```
<add name="DefaultConnection" connectionString="Server=localhost;Database=finalwork;Uid=root;Pwd=;AllowUserVariables=True;" providerName="MySql.Data.MySqlClient" />
```

## Лицензия

Данный проект защищается лицензией GNU GPLv3 - ознакомиться с ней можно [здесь](LICENSE.md)

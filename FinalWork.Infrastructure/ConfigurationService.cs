﻿using System;
using System.Configuration;

namespace FinalWork.Infrastructure
{
    class ConfigurationService 
        : IConfigurationService
    {
        public string GetAppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public string GetPathToConfiguration()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        }

        public object GetSection(string sectionName)
        {
            return ConfigurationManager.GetSection(sectionName);
        }
    }
}

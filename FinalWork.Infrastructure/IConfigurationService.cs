﻿namespace FinalWork.Infrastructure
{
    /// <summary>
    /// Интерфейс сервиса конфигурации.
    /// </summary>
    public interface IConfigurationService
    {
        /// <summary>
        /// Получение пути до конфигурации компонента
        /// </summary>
        /// <param name="serviceType">Тип сервиса.</param>
        /// <returns>Конфигурация.</returns>
        string GetPathToConfiguration();

        /// <summary>
        /// Получение значения из секции appSettings.
        /// </summary>
        /// <param name="key">Имя параметра.</param>
        /// <returns>Значение параметра.</returns>
        string GetAppSetting(string key);

        /// <summary>
        /// Получение секции из файла конфигурации
        /// </summary>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        object GetSection(string sectionName);
    }
}
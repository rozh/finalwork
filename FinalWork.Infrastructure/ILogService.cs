﻿using System;

namespace FinalWork.Infrastructure
{
    /// <summary>
    /// Интерфейс сервиса логгирования.
    /// </summary>
    public interface ILogService
    {
        /// <summary>
        /// Получение логгера для указаного источника.
        /// </summary>
        /// <param name="source">Имя источника.</param>
        /// <returns>Интерфейс логгера.</returns>
        ILogger GetLogger(string source);

        /// <summary>
        /// Получение логгера для указаного источника.
        /// </summary>
        /// <param name="source">Тип объекта источника.</param>
        /// <returns>Интерфейс логгера.</returns>
        ILogger GetLogger(Type sourceType);

        /// <summary>
        /// Получение логгера для указаного источника.
        /// </summary>
        /// <param name="source">Тип объекта источника.</param>
        /// <param name="instanceName">Имя экземпляра объекта.</param>
        /// <returns>Интерфейс логгера.</returns>
        ILogger GetLogger(Type sourceType, string instanceName);
    }
}
﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;

namespace FinalWork.Infrastructure
{
    /// <summary>
    /// Реализация паттерна ServiceLocator <see cref="http://martinfowler.com/articles/injection.html"/>
    /// Использует паттер Singleton.
    /// </summary>
    /// <example>
    /// Для использования сервиса можно использовать следующий код
    /// <code>
    /// ServiceLocator.Instance.Service.Method();
    /// </code>
    /// </example>
    public sealed class ServiceLocator
    {
        /// <summary>
        /// Единственный экземпляр
        /// </summary>
        private static ServiceLocator _instance;

        public static ServiceLocator Instance => _instance ?? (_instance = new ServiceLocator());

        private ServiceLocator()
        { }

        /// <summary>
        /// Создание сервиса указанного типа, используя данные из конфигурации.
        /// </summary>
        /// <typeparam name="TServiceType">Тип сервиса.</typeparam>
        /// <returns>
        /// Экземпляр созданного сервиса.
        /// </returns>
        public TServiceType CreateService<TServiceType>()
            where TServiceType : class
        {
            var keyName = typeof(TServiceType).ToString();
            var conf = ConfigurationManager.GetSection("FinalWork.baseServices") as NameValueCollection;
            if (conf != null && conf.AllKeys.Contains(keyName))
            {
                var className = conf[keyName];

                var serviceInstance = Activator.CreateInstance(Type.GetType(className));
                if (!(serviceInstance is TServiceType))
                {
                    throw new ConfigurationErrorsException($"Класс {className} не реализует интерфейс {keyName}");
                }
                return serviceInstance as TServiceType;
            }
            else
            {
                throw new ConfigurationErrorsException($"В конфигурации не найден сервис реализующий интерфейс {keyName}");
            }
        }

        /// <summary>
        /// Экземпляр сервиса логгирования.
        /// </summary>
        private ILogService _logService;

        /// <summary>
        /// Сервис логгирования.
        /// </summary>
        public ILogService LogService
        {
            get
            {
                if (_logService == null)
                {
                    lock (this)
                    {
                        if (_logService == null)
                        {
                            _logService = CreateService<ILogService>();
                        }
                    }
                }
                return _logService;
            }
        }

        /// <summary>
        /// Экземпляр сервиса конфигурации.
        /// </summary>
        private IConfigurationService _configurationService;

        /// <summary>
        /// Сервис конфигурации.
        /// </summary>
        public IConfigurationService ConfigurationService
        {
            get
            {
                if (_configurationService == null)
                {
                    lock (this)
                    {
                        if (_configurationService == null)
                        {
                            _configurationService = CreateService<IConfigurationService>();
                        }
                    }
                }
                return _configurationService;
            }
        }
    }
}
﻿using System;

namespace FinalWork.Infrastructure
{
    /// <summary>
    /// Интерфейс логгера.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Логирование трассировочного сообщения
        /// </summary>
        /// <param name="message"></param>
        void Trace(string message);

        /// <summary>
        /// Логирование отладочного сообщения.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        void Debug(string message);

        /// <summary>
        /// Логирование информационного сообщения.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        void Info(string message);

        /// <summary>
        /// Логирование предупреждения.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        void Warning(string message);

        /// <summary>
        /// Логирование ошибки.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        void Error(string message);

        /// <summary>
        /// Логирование ошибки.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        /// <param name="ex">Исключение.</param>
        void Error(string message, Exception ex);

        /// <summary>
        /// Логирование ошибки.
        /// </summary>
        /// <param name="ex">Исключение.</param>
        void Error(Exception ex);

        /// <summary>
        /// Логирование фатальной ошибки
        /// </summary>
        /// <param name="message"></param>
        void Fatal(string message);

        /// <summary>
        /// Логирование фатальной ошибки
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        void Fatal(string message, Exception ex);

        
        /// <summary>
        /// Логгирование фатальной ошибки
        /// </summary>
        /// <param name="ex"></param>
        void Fatal(Exception ex);

    }
}
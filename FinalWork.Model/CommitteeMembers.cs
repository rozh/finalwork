﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     Члены комиссии
    /// </summary>
    [Table("CommitteeMembers")]
    public class CommitteeMembers
        : ModelBase
    {
        /// <summary>
        ///     Guid комиссии
        /// </summary>
        [MaxLength(36)]
        public string CommitteeId { get; set; }

        /// <summary>
        ///     Комиссия
        /// </summary>
        [ForeignKey("CommitteeId")]
        public Committee Committee { get; set; }

        /// <summary>
        ///     Guid члена комиссии
        /// </summary>
        [MaxLength(36)]
        public string CommitteeMemberId { get; set; }

        /// <summary>
        ///     Член комиссии
        /// </summary>
        [ForeignKey("CommitteeMemberId")]
        public CommitteeMember CommitteeMember { get; set; }
    }
}
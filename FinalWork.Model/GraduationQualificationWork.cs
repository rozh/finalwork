﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     ВКР
    /// </summary>
    [Table("GraduationQualificationWork")]
    public class GraduationQualificationWork
        : ModelBase
    {
        /// <summary>
        ///     Тема ВКР
        /// </summary>
        [DisplayName("Тема ВКР")]
        [MaxLength(255)]
        public string TopicName { get; set; }

        /// <summary>
        ///     Руководитель
        /// </summary>
        [DisplayName("Руководитель")]
        [ForeignKey("SupervisorId")]
        public virtual Person Supervisor { get; set; }

        /// <summary>
        ///     Guid руководителя
        /// </summary>
        [MaxLength(36)]
        public string SupervisorId { get; set; }

        /// <summary>
        ///     Рецензент
        /// </summary>
        [DisplayName("Рецензент")]
        [ForeignKey("ReviewerId")]
        public virtual Person Reviewer { get; set; }

        /// <summary>
        ///     Guid Рецензента
        /// </summary>
        [MaxLength(36)]
        public string ReviewerId { get; set; }


        /// <summary>
        ///     Guid студента
        /// </summary>
        [MaxLength(36)]
        public string StudentId { get; set; }

        /// <summary>
        ///     Студент
        /// </summary>
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
    }
}
﻿using System;
using System.ComponentModel;
using FinalWork.Model.Interfaces;

namespace FinalWork.Model.Base
{
    public class CatalogBase
        : ModelBase
        , ISoftDeleted
    {
        public CatalogBase()
        {
            CreateDate = DateTime.Now;
        }

        /// <summary>
        ///     Время создания
        /// </summary>
        [DisplayName("Время создания")]
        [Description("Время, когда объект был создан")]
        public DateTime? CreateDate { get; set; }

        /// <summary>
        ///     Время обновления
        /// </summary>
        [DisplayName("Время обновления")]
        [Description("Время, когда объект был обновлен")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        ///     Признак удаления
        /// </summary>
        public bool Deleted { get; set; }
    }
}
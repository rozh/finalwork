﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FinalWork.Model.Base
{
    public class CatalogDeclensionPersonBase
        : CatalogPersonBase
    {
        #region Dative

        /// <summary>
        ///     Имя в дательном падеже
        /// </summary>
        [DisplayName("Имя (д.п.)")]
        [MaxLength(100)]
        public string DativeFirstName { get; set; }

        /// <summary>
        ///     Отчество в дательном падеже
        /// </summary>
        [DisplayName("Отчество (д.п.)")]
        [MaxLength(100)]
        public string DativeMiddleName { get; set; }

        /// <summary>
        ///     Фамилия в дательном падеже
        /// </summary>
        [DisplayName("Фамилия (д.п.)")]
        [MaxLength(100)]
        public string DativeLastName { get; set; }

        /// <summary>
        ///     Фамилия и инициалы в дательном падеже
        /// </summary>
        [DisplayName("Фамилия И.О (д.п.)")]
        public string DativeFullNameShort
            =>
                (DativeLastName?.Length > 0 ? $"{DativeLastName} " : "") +
                (DativeFirstName?.Length > 0 ? $"{DativeFirstName[0]}. " : "") +
                (DativeMiddleName?.Length > 0 ? $"{DativeMiddleName[0]}." : "");

        /// <summary>
        ///     Фамилия Имя Отчество в дательном падеже
        /// </summary>
        [DisplayName("ФИО (д.п.)")]
        public string DativeFullName => $"{DativeLastName} {DativeFirstName} {DativeMiddleName}";

        #endregion

        #region Genitive

        /// <summary>
        ///     Имя в родительном падеже
        /// </summary>
        [DisplayName("Имя (р.п.)")]
        [MaxLength(100)]
        public string GenitiveFirstName { get; set; }

        /// <summary>
        ///     Отчество в родительном падеже
        /// </summary>
        [DisplayName("Отчество (р.п.)")]
        [MaxLength(100)]
        public string GenitiveMiddleName { get; set; }

        /// <summary>
        ///     Фамилия в родительном падеже
        /// </summary>
        [DisplayName("Фамилия (р.п.)")]
        [MaxLength(100)]
        public string GenitiveLastName { get; set; }

        /// <summary>
        ///     Фамилия и инициалы в родительном падеже
        /// </summary>
        [DisplayName("Фамилия И.О (р.п.)")]
        public string GenitiveFullNameShort
            =>
                (GenitiveLastName?.Length > 0 ? $"{GenitiveLastName} " : "") +
                (GenitiveFirstName?.Length > 0 ? $"{GenitiveFirstName[0]}. " : "") +
                (GenitiveMiddleName?.Length > 0 ? $"{GenitiveMiddleName[0]}." : "");

        /// <summary>
        ///     Фамилия Имя Отчество в родительном падеже
        /// </summary>
        [DisplayName("ФИО (р.п.)")]
        public string GenitiveFullName => $"{GenitiveLastName} {GenitiveFirstName} {GenitiveMiddleName}";

        #endregion

        #region Instrumental
        
        /// <summary>
        ///     Имя в творительном падеже
        /// </summary>
        [DisplayName("Имя (т.п.)")]
        [MaxLength(100)]
        public string InstrumentalFirstName { get; set; }

        /// <summary>
        ///     Отчество в творительном падеже
        /// </summary>
        [DisplayName("Отчество (т.п.)")]
        [MaxLength(100)]
        public string InstrumentalMiddleName { get; set; }

        /// <summary>
        ///     Фамилия в творительном падеже
        /// </summary>
        [DisplayName("Фамилия (т.п.)")]
        [MaxLength(100)]
        public string InstrumentalLastName { get; set; }

        /// <summary>
        ///     Фамилия и инициалы в творительном падеже
        /// </summary>
        [DisplayName("Фамилия И.О (т.п.)")]
        public string InstrumentalFullNameShort
            =>
                (InstrumentalLastName?.Length > 0 ? $"{InstrumentalLastName} " : "") +
                (InstrumentalFirstName?.Length > 0 ? $"{InstrumentalFirstName[0]}. " : "") +
                (InstrumentalMiddleName?.Length > 0 ? $"{InstrumentalMiddleName[0]}." : "");

        /// <summary>
        ///     Фамилия Имя Отчество в творительном падеже
        /// </summary>
        [DisplayName("ФИО (т.п.)")]
        public string InstrumentalFullName => $"{InstrumentalLastName} {InstrumentalFirstName} {InstrumentalMiddleName}";

        #endregion

    }
}
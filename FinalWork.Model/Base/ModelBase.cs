﻿using System;
using System.ComponentModel.DataAnnotations;
using FinalWork.Model.Interfaces;

namespace FinalWork.Model.Base
{
    public class ModelBase 
        : IHaveId
    {
        public ModelBase()
        {
            Id = Guid.NewGuid().ToString("D");
        }

        [Key, MaxLength(36)]
        public string Id { get; set; }
    }
}
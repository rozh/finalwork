﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FinalWork.Model.Base
{
    public class CatalogPersonBase
        : CatalogBase
    {
        /// <summary>
        ///     Имя
        /// </summary>
        [DisplayName("Имя")]
        [MaxLength(100)]
        public string FirstName { get; set; }

        /// <summary>
        ///     Отчество
        /// </summary>
        [DisplayName("Отчество")]
        [MaxLength(100)]
        public string MiddleName { get; set; }

        /// <summary>
        ///     Фамилия
        /// </summary>
        [DisplayName("Фамилия")]
        [MaxLength(100)]
        public string LastName { get; set; }

        /// <summary>
        ///     Фамилия и инициалы
        /// </summary>
        [DisplayName("Фамилия И.О")]
        public string FullNameShort
            =>
                (LastName.Length > 0 ? $"{LastName} " : "") +
                (FirstName.Length > 0 ? $"{FirstName[0]}. " : "") +
                (MiddleName.Length > 0 ? $"{MiddleName[0]}." : "");

        /// <summary>
        ///     Фамилия Имя Отчество
        /// </summary>
        [DisplayName("ФИО")]
        public string FullName => $"{LastName} {FirstName} {MiddleName}";
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     Человек
    /// </summary>
    [Table("Person")]
    public class Person
        : CatalogDeclensionPersonBase
    {
        public Person(string firstName, string middleName, string lastName)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
        }

        public Person()
        {
            FirstName = string.Empty;
            MiddleName = string.Empty;
            LastName = string.Empty;
        }

        /// <summary>
        ///     Ученая степень
        /// </summary>
        [DisplayName("Ученая степень")]
        [MaxLength(255)]
        public string AcademicDegree { get; set; }

        /// <summary>
        ///     Ученое звание
        /// </summary>
        [DisplayName("Ученое звание")]
        [MaxLength(255)]
        public string AcademicRank { get; set; }

        /// <summary>
        ///     Организация
        /// </summary>
        [DisplayName("Организация")]
        [MaxLength(255)]
        public string Organization { get; set; }

        /// <summary>
        ///     Должность
        /// </summary>
        [DisplayName("Должность")]
        [MaxLength(255)]
        public string Position { get; set; }
    }
}
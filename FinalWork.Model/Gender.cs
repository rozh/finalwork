﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     Пол
    /// </summary>
    [Table("gender")]
    public class Gender
        : CatalogBase
    {
        /// <summary>
        ///     Название
        /// </summary>
        [DisplayName("Название")]
        [MaxLength(255)]
        public string Name { get; set; }

        /// <summary>
        ///     Короткое название 
        /// </summary>
        [DisplayName("Короткое название")]
        [MaxLength(50)]
        public string ShortName { get; set; }
    }
}
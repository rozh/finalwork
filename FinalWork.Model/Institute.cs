﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    [Table("institute")]
    public class Institute : CatalogBase
    {
        [Required]
        [DisplayName("Название института")]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [DisplayName("Короткое название института")]
        [MaxLength(50)]
        public string ShortName { get; set; }
        
        /// <summary>
        /// Кафедры
        /// </summary>
        public virtual ICollection<Departament> Departaments { get; set; }
    }
}
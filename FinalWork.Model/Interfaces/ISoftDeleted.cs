﻿namespace FinalWork.Model.Interfaces
{
    public interface ISoftDeleted
    {
        bool Deleted { get; set; }
    }
}
﻿using System;

namespace FinalWork.Model.Interfaces
{
    internal interface IEndYear
    {
        /// <summary>
        /// Год защиты
        /// </summary>
        DateTime? EndYear { get; set; }
    }
}
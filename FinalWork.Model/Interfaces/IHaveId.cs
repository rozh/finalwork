﻿namespace FinalWork.Model.Interfaces
{
    internal interface IHaveId
    {
        string Id { get; set; }
    }
}
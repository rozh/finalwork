﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;
using FinalWork.Model.Interfaces;

namespace FinalWork.Model
{
    /// <summary>
    ///     Защиты
    /// </summary>
    [Table("defense")]
    public class Defense
        : ModelBase, IEndYear
    {
        /// <summary>
        ///     № протокола
        /// </summary>
        [DisplayName("№ протокола")]
        public int ProtocolNumber { get; set; }

        /// <summary>
        ///     Дата защиты
        /// </summary>
        [DisplayName("Дата защиты")]
        public DateTime? DefenseDate { get; set; }

        /// <summary>
        ///    Время начала защиты
        /// </summary>
        [DisplayName("Время начала защиты")]
        public DateTime? DefenseStartTime { get; set; }

        /// <summary>
        ///    Время окончания защиты
        /// </summary>
        [DisplayName("Время окончания защиты")]
        public DateTime? DefenseEndTime { get; set; }

        /// <summary>
        ///     Guid комиссии
        /// </summary>
        [MaxLength(36)]
        public string CommitteeId { get; set; }

        /// <summary>
        ///     Комиссия
        /// </summary>
        [ForeignKey("CommitteeId")]
        public virtual Committee Committee { get; set; }

        /// <summary>
        ///     Guid ВКР
        /// </summary>
        [MaxLength(36)]
        public string GraduationQualificationWorkId { get; set; }

        /// <summary>
        ///     ВКР
        /// </summary>
        [ForeignKey("GraduationQualificationWorkId")]
        public virtual GraduationQualificationWork GraduationQualificationWork { get; set; }

        /// <summary>
        /// Вопросы по защите
        /// </summary>
        public virtual ICollection<DefenseQuestion> Questions { get; set; }

        /// <summary>
        /// Год защиты
        /// </summary>
        public DateTime? EndYear { get; set; }
    }
}
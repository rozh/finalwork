﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     Кафедра
    /// </summary>
    [Table("departament")]
    public class Departament
        : CatalogBase
    {
        /// <summary>
        ///     Название
        /// </summary>
        [DisplayName("Название")]
        [MaxLength(255)]
        public string Name { get; set; }

        /// <summary>
        ///     Короткое название 
        /// </summary>
        [DisplayName("Короткое название")]
        [MaxLength(50)]
        public string ShortName { get; set; }
        
        /// <summary>
        ///     Guid института
        /// </summary>
        [MaxLength(36)]
        public string InstituteId { get; set; }

        /// <summary>
        ///     Институт
        /// </summary>
        [ForeignKey("InstituteId")]
        public virtual Institute Institute { get; set; }
    }
}
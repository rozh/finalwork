﻿namespace FinalWork.Model
{
    public static class Constants
    {
        public const string TemplateDirKey = "TEMPLATE_DIR";
        public const string ProtocolTemplateNameKey = "PROTOCOL_TEMPLATE_NAME";

        public static class Ids
        {
            public static class GenderId
            {
                public const string Female = "2927437c-bce4-43c1-b7fd-520f36380001";
                public const string Male = "2927437c-bce4-43c1-b7fd-520f36380002";
            }
        }
    }
}

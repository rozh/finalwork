﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     Вопрос на защите
    /// </summary>
    [Table("DefenseQuestion")]
    public class DefenseQuestion
        : ModelBase
    {
        /// <summary>
        ///     Член комиссии
        /// </summary>
        [ForeignKey("CommitteeMemberId")]
        public virtual CommitteeMember CommitteeMember { get; set; }

        /// <summary>
        ///     Guid члена комиссии
        /// </summary>
        [MaxLength(36)]
        public string CommitteeMemberId { get; set; }

        /// <summary>
        ///     Вопрос
        /// </summary>
        [DisplayName("Вопрос")]
        [MaxLength(255)]
        public string Question { get; set; }
    }
}
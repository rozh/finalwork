﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Text;
using FinalWork.Model.Base;
using FinalWork.Model.Enums;

namespace FinalWork.Model
{
    /// <summary>
    ///     Пользователь
    /// </summary>
    [Table("user")]
    public class User 
        : CatalogPersonBase
    {
        public User(string login, string pass, UserAccessLevel accessLevel)
        {
            Login = login;
            Password = pass;
            AccessLevel = accessLevel;
        }

        public User()
        { }

        /// <summary>
        ///     Логин
        /// </summary>
        [MaxLength(50)]
        public string Login { get; set; }
        
        /// <summary>
        ///     Время последнего логина
        /// </summary>
        public DateTime? AccessDate { get; set; }

        /// <summary>
        ///     <para>Пароль</para>
        ///     <para>При присвоении получает MD5 хеш от строки и хранит его</para>
        /// </summary>
        [MaxLength(50)]
        public string Password { get; set; }

        /// <summary>
        ///     Уровень доступа
        /// </summary>
        public UserAccessLevel AccessLevel { get; set; }

        public static string CalculateMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();

            foreach (var t in hash)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
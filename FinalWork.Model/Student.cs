﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;
using FinalWork.Model.Interfaces;

namespace FinalWork.Model
{
    /// <summary>
    ///     Студент
    /// </summary>
    [Table("student")]
    public class Student
        : CatalogDeclensionPersonBase, IEndYear
    {
        public Student(string firstName, string middleName, string lastName)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
        }

        public Student()
        {
            FirstName = string.Empty;
            MiddleName = string.Empty;
            LastName = string.Empty;
        }
        
        /// <summary>
        ///     Группа
        /// </summary>
        [DisplayName("Группа")]
        [MaxLength(10)]
        public string Group { get; set; }

        /// <summary>
        ///     Форма обучения
        /// </summary>
        [DisplayName("Форма обучения")]
        [MaxLength(10)]
        public string EducationType { get; set; }
        
        /// <summary>
        ///     Guid института
        /// </summary>
        [MaxLength(36)]
        public string InstituteId { get; set; }

        /// <summary>
        ///     Институт
        /// </summary>
        [ForeignKey("InstituteId")]
        public virtual Institute Institute { get; set; }

        /// <summary>
        ///     Guid кафедры
        /// </summary>
        [MaxLength(36)]
        public string DepartamentId { get; set; }

        /// <summary>
        ///     Кафедра
        /// </summary>
        [ForeignKey("DepartamentId")]
        public virtual Departament Departament { get; set; }
        
        /// <summary>
        ///     Email
        /// </summary>
        [DisplayName("Email")]
        [MaxLength(255)]
        public string Email { get; set; }

        /// <summary>
        ///     Образование
        /// </summary>
        [DisplayName("EducationStep")]
        [MaxLength(50)]
        public string EducationStep { get; set; }

        /// <summary>
        /// Год защиты
        /// </summary>
        public DateTime? EndYear { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace FinalWork.Model.Enums
{
    public enum UserAccessLevel : short
    {
        [Display(Name = "Нет доступа")] None = 0,
        [Display(Name = "Администратор")] Administrator = 10,
        [Display(Name = "Диспетчер")] Dispatcher = 20,
        [Display(Name = "Руководитель/Мастер")] Superviser = 30,
        [Display(Name = "Охрана")] Driver = 40
    }
}
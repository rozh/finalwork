﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     Комиссия
    /// </summary>
    [Table("Committee")]
    public sealed class Committee
        : ModelBase
    {
        public Committee()
        {
            CommitteeMembers = new HashSet<CommitteeMember>();
        }

        /// <summary>
        ///     Guid председателя комиссии
        /// </summary>
        [MaxLength(36)]
        public string CommitteeGeneralId { get; set; }

        /// <summary>
        ///     Председатель комиссии
        /// </summary>
        [ForeignKey("CommitteeGeneralId")]
        public CommitteeMember CommitteeGeneral { get; set; }

        /// <summary>
        ///     Guid секретаря комиссии
        /// </summary>
        [MaxLength(36)]
        public string CommitteeSecretaryId { get; set; }

        /// <summary>
        ///     Секретарь комиссии
        /// </summary>
        [ForeignKey("CommitteeSecretaryId")]
        public CommitteeMember CommitteeSecretary { get; set; }

        /// <summary>
        ///     Члены комиссии
        /// </summary>
        public ICollection<CommitteeMember> CommitteeMembers { get; set; }
    }
}
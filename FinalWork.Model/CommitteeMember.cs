﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     Член комиссии
    /// </summary>
    [Table("CommitteeMember")]
    public class CommitteeMember
        : ModelBase
    {
        /// <summary>
        ///     Человек
        /// </summary>
        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }

        /// <summary>
        ///     Guid человека
        /// </summary>
        [MaxLength(36)]
        public string PersonId { get; set; }

        /// <summary>
        /// Комиссии
        /// </summary>
        public virtual ICollection<Committee> Committies { get; set; }
    }
}
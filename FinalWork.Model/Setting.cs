﻿using System.ComponentModel.DataAnnotations.Schema;
using FinalWork.Model.Base;

namespace FinalWork.Model
{
    /// <summary>
    ///     Настройки
    /// </summary>
    [Table("setting")]
    public class Setting
        : ModelBase
    {
        public Setting(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public Setting() : this(string.Empty, string.Empty)
        {
        }

        /// <summary>
        ///     Ключ
        /// </summary>
        [Column("key")]
        public string Key { get; set; }

        /// <summary>
        ///     Значение
        /// </summary>
        [Column("value")]
        public string Value { get; set; }
    }
}
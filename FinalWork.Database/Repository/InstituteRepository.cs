﻿using System.Linq;
using FinalWork.Database.Context;
using System.Collections.Generic;
using System.Data.Entity;
using FinalWork.Database.Repository.Base;
using FinalWork.Model;

namespace FinalWork.Database.Repository
{
    /// <summary>
    ///     Репозиторий институтов
    /// </summary>
    public class InstituteRepository : GenericRepository<Institute>
    {
        public InstituteRepository(DatabaseContext context) : base(context)
        {
        }

        public List<Institute> GetAllInstituts()
        {
            var instituts = DbSet.Include(s=>s.Departaments).ToList();
            return instituts;
        }

    }
}
﻿using System.Linq;
using FinalWork.Database.Context;
using FinalWork.Database.Repository.Base;
using FinalWork.Model;

namespace FinalWork.Database.Repository
{
    public class SettingRepository : GenericRepository<Setting>
    {
        public SettingRepository(DatabaseContext context) : base(context)
        {
        }

        /// <summary> Выдавать набор значений по заданному идентификатору </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetSetting(string key)
        {
            var setting = DbSet.FirstOrDefault(p => p.Key == key);
            if (setting == null) return string.Empty;
            return setting.Value;
        }

        /// <summary> Выдавать набор значений по заданным идентификаторам </summary>
        /// <param name="keys"></param>
        public Setting[] GetSetting(string[] keys)
        {
            return DbSet.Where(s => keys.Contains(s.Key)).ToArray();
        }
    }
}
﻿using FinalWork.Database.Context;
using FinalWork.Database.Repository.Base;
using FinalWork.Model;

namespace FinalWork.Database.Repository
{
    /// <summary>
    ///     Репозиторий людей
    /// </summary>
    public class PersonRepository : GenericRepository<Person>
    {
        public PersonRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
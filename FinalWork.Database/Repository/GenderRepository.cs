﻿using FinalWork.Database.Context;
using FinalWork.Database.Repository.Base;
using FinalWork.Model;

namespace FinalWork.Database.Repository
{
    /// <summary>
    ///     Репозиторий полов
    /// </summary>
    public class GenderRepository : GenericRepository<Gender>
    {
        public GenderRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
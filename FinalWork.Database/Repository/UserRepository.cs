﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FinalWork.Database.Context;
using FinalWork.Database.Repository.Base;
using FinalWork.Model;

namespace FinalWork.Database.Repository
{
    /// <summary>
    ///     Репозиторий пользователей
    /// </summary>
    public class UserRepository : GenericRepository<User>
    {
        public UserRepository(DatabaseContext context) : base(context)
        {
        }

        public bool CheckAuth(User user)
        {
            //string md5pass = CalculateMD5Hash(pass);
            return Get(u => u.Login == user.Login && u.Password == user.Password).Any();
        }
        public User GetUser(string login, string pass)
        {
            //string md5pass = CalculateMD5Hash(pass);
            return Get(u => u.Login == login && u.Password == pass).FirstOrDefault();
        }

        public async Task<bool> CheckLoginAsync(User user)
        {
            return await Task<bool>.Factory.StartNew(() =>
            {
                var result = DbSet.AnyAsync(u => u.Login == user.Login && u.Id != user.Id);
                return result.Result;
            });
        }
    }
}
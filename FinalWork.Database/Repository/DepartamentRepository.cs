﻿using System.Linq;
using FinalWork.Database.Context;
using System.Collections.Generic;
using System.Data.Entity;
using FinalWork.Database.Repository.Base;
using FinalWork.Model;

namespace FinalWork.Database.Repository
{
    /// <summary>
    ///     Репозиторий кафедр
    /// </summary>
    public class DepartamentRepository : GenericRepository<Departament>
    {
        public DepartamentRepository(DatabaseContext context) : base(context)
        {
        }

        public List<Departament> GetAllDepartaments()
        {
            var departaments = DbSet.Include(s=>s.Institute).ToList();
            return departaments;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FinalWork.Database.Context;
using FinalWork.Database.Interfaces;
using FinalWork.Model.Base;

namespace FinalWork.Database.Repository.Base
{
    /// <summary>
    ///     общий репозиторий
    /// </summary>
    /// <typeparam name="TEntity">Модель сущности БД</typeparam>
    public class GenericRepository<TEntity>
        : IGenericRepositorySave, IDisposable, IGenericRepository<TEntity> where TEntity : class
    {
        internal DatabaseContext Context { get; }

        internal DbSet<TEntity> DbSet { get; }

        public GenericRepository(DatabaseContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }
        
        /// <summary>
        ///     Получить элементы
        /// </summary>
        /// <param name="filter">фильтры</param>
        /// <param name="orderBy">сортировка</param>
        /// <param name="includeProperties">включить другие таблицы</param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            var query = GetQuery(filter, orderBy, includeProperties);

            return query.ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            var query = GetQuery(filter, orderBy, includeProperties);

            var list = await query.AsNoTracking().ToListAsync();
            return list;
        }

        /// <summary>
        /// Для формирования запросов
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> Query()
        {
            return DbSet.AsQueryable();
        }

        /// <summary>
        /// Для формирования запросов
        /// </summary>
        /// <returns></returns>
        public virtual void Reload()
        {
            Context.Entry(DbSet).Reload();
        }

        /// <summary>
        ///     получение элемента по Id
        /// </summary>
        /// <param name="id">Id элемента</param>
        /// <returns></returns>
        public virtual TEntity GetById(object id)
        {
            return DbSet.Find(id);
        }

        public virtual async Task<TEntity> GetByIdAsync(object id)
        {
            return await Task<TEntity>.Factory.StartNew(() =>
            {
                var entity = DbSet.FindAsync(id);
                return entity.Result;
            });
            
        }

        /// <summary>
        ///     Добавление элемента
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        /// <summary>
        ///     Добавление или обновление элемента
        /// </summary>
        /// <param name="entity"></param>
        public virtual void AddOrUpdate(TEntity entity)
        {
            DbSet.AddOrUpdate(entity);
        }

        /// <summary>
        ///     Удаление элемента по Id
        /// </summary>
        /// <param name="id">Id элемента</param>
        public virtual void Delete(object id)
        {
            var entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        ///     удаление элемента
        /// </summary>
        /// <param name="entityToDelete">элемент</param>
        public virtual void Delete(TEntity entityToDelete)
        {
            if (Context.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }

            DbSet.Remove(entityToDelete);
        }

        /// <summary>
        ///     обновление элемента
        /// </summary>
        /// <param name="entityToUpdate"></param>
        public virtual void Update(TEntity entityToUpdate)
        {
            
            //if (Context.Entry(entityToUpdate).State == EntityState.Detached)
            //{
            //    DbSet.Attach(entityToUpdate);
            //}

            Context.Entry(entityToUpdate).State = EntityState.Modified;

            var catalogBase = entityToUpdate as CatalogBase;
            if (catalogBase != null)
            {
                catalogBase.UpdateDate = DateTime.Now;
            }
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return Context.SaveChangesAsync();
        }

        private IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return orderBy != null ? orderBy(query) : query;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
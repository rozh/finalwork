﻿using System.Linq;
using FinalWork.Database.Context;
using System.Collections.Generic;
using System.Data.Entity;
using FinalWork.Database.Repository.Base;
using FinalWork.Model;

namespace FinalWork.Database.Repository
{
    /// <summary>
    ///     Репозиторий студентов
    /// </summary>
    public class StudentRepository : GenericRepository<Student>
    {
        public StudentRepository(DatabaseContext context) : base(context)
        {
        }

        public List<Student> GetAllStudents()
        {
            var students = DbSet.Include(s=>s.Institute).Include(s=>s.Departament).ToList();
            return students;
        }

    }
}
﻿using System.Threading.Tasks;

namespace FinalWork.Database.Interfaces
{
    public interface IGenericRepositorySave
    {
        void Reload();
        void SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
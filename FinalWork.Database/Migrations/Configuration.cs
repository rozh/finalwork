﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using FinalWork.Model;
using FinalWork.Model.Enums;

namespace FinalWork.Database.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Context.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations";
            CommandTimeout = 20000;
        }

        protected override void Seed(Context.DatabaseContext context)
        {
            context.Settings.AddIfNotExists(
                new Setting() { Id = "d9df8a6a-ba14-11e6-8281-00fff2a40001", Key = Constants.TemplateDirKey, Value = "templates" },
                new Setting() { Id = "d9df8a6a-ba14-11e6-8281-00fff2a40002", Key = Constants.ProtocolTemplateNameKey, Value = "protocol.rtf" }
            );

            context.Users.AddIfNotExists(new User()
            {
                Id = "9927437c-bce4-43c1-b7fd-520f3638aa1a",
                Login = "Администратор",
                Password = "21232F297A57A5A743894A0E4A801FC3", //admin
                FirstName = "Администратор",
                LastName = "Администратор",
                AccessDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                MiddleName = "Администратор",
                AccessLevel = UserAccessLevel.Administrator
            });

            context.Institutes.AddIfNotExists(new Institute()
            {
                Id = "0927437c-bce4-43c1-b7fd-520f36380001",
                Name = "ИКТЗИ",
                ShortName = "ИКТЗИ",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
            });

            context.Departaments.AddIfNotExists(new Departament()
            {
                Id = "1927437c-bce4-43c1-b7fd-520f36380001",
                Name = "Компьютерных систем",
                ShortName = "КС",
                InstituteId = "0927437c-bce4-43c1-b7fd-520f36380001",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
            });

            context.Genders.AddIfNotExists(new Gender()
            {
                Id = Constants.Ids.GenderId.Female,
                Name = "Женский",
                ShortName = "ж",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
            }, new Gender()
            {
                Id = Constants.Ids.GenderId.Male,
                Name = "Мужской",
                ShortName = "м",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
            });
            
            context.Students.AddIfNotExists(new Student()
            {
                Id = "9927437c-bce4-43c1-b7fd-520f36380022",
                FirstName = "Тест",
                LastName = "Тестовый",
                MiddleName = "Тестович",
                DativeFirstName = "Тесту",
                DativeLastName = "Тестовому",
                DativeMiddleName = "Тестовичу",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                InstituteId = "0927437c-bce4-43c1-b7fd-520f36380001",
                DepartamentId = "1927437c-bce4-43c1-b7fd-520f36380001",
                Group = "4434",
            });

            var com = new Committee()
            {
                Id = "9927447c-bce4-43c1-b7fd-520f36380022",
                CommitteeGeneral = new CommitteeMember()
                {
                    Person = new Person("Член", "Комиссии", "Главный"),
                },
                CommitteeSecretary = new CommitteeMember()
                {
                    Person = new Person("Комиссии", "Секретарь", ""),
                },
                CommitteeMembers = new List<CommitteeMember>()
                {
                    new CommitteeMember()
                    {
                        Person = new Person("Еще", "Один", "Член комиссии"),
                    },

                    new CommitteeMember()
                    {
                        Person = new Person("Другой", "Второй", "Член комиссии"),
                    }
                }
            };

            context.Committis.AddIfNotExists(com);

            context.SaveChanges();
        }
    }
}

namespace FinalWork.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommitteeMember",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        PersonId = c.String(maxLength: 36, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Committee",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        CommitteeGeneralId = c.String(maxLength: 36, storeType: "nvarchar"),
                        CommitteeSecretaryId = c.String(maxLength: 36, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CommitteeMember", t => t.CommitteeGeneralId)
                .ForeignKey("dbo.CommitteeMember", t => t.CommitteeSecretaryId)
                .Index(t => t.CommitteeGeneralId)
                .Index(t => t.CommitteeSecretaryId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        AcademicDegree = c.String(maxLength: 255, storeType: "nvarchar"),
                        AcademicRank = c.String(maxLength: 255, storeType: "nvarchar"),
                        Organization = c.String(maxLength: 255, storeType: "nvarchar"),
                        Position = c.String(maxLength: 255, storeType: "nvarchar"),
                        DativeFirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        DativeMiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        DativeLastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        GenitiveFirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        GenitiveMiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        GenitiveLastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        InstrumentalFirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        InstrumentalMiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        InstrumentalLastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        FirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        MiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        LastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        CreateDate = c.DateTime(precision: 0),
                        UpdateDate = c.DateTime(precision: 0),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DefenseQuestion",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        CommitteeMemberId = c.String(maxLength: 36, storeType: "nvarchar"),
                        Question = c.String(maxLength: 255, storeType: "nvarchar"),
                        Defense_Id = c.String(maxLength: 36, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CommitteeMember", t => t.CommitteeMemberId)
                .ForeignKey("dbo.defense", t => t.Defense_Id)
                .Index(t => t.CommitteeMemberId)
                .Index(t => t.Defense_Id);
            
            CreateTable(
                "dbo.defense",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        ProtocolNumber = c.Int(nullable: false),
                        DefenseDate = c.DateTime(precision: 0),
                        DefenseStartTime = c.DateTime(precision: 0),
                        DefenseEndTime = c.DateTime(precision: 0),
                        CommitteeId = c.String(maxLength: 36, storeType: "nvarchar"),
                        GraduationQualificationWorkId = c.String(maxLength: 36, storeType: "nvarchar"),
                        EndYear = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Committee", t => t.CommitteeId)
                .ForeignKey("dbo.GraduationQualificationWork", t => t.GraduationQualificationWorkId)
                .Index(t => t.CommitteeId)
                .Index(t => t.GraduationQualificationWorkId);
            
            CreateTable(
                "dbo.GraduationQualificationWork",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        TopicName = c.String(maxLength: 255, storeType: "nvarchar"),
                        SupervisorId = c.String(maxLength: 36, storeType: "nvarchar"),
                        ReviewerId = c.String(maxLength: 36, storeType: "nvarchar"),
                        StudentId = c.String(maxLength: 36, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.ReviewerId)
                .ForeignKey("dbo.student", t => t.StudentId)
                .ForeignKey("dbo.Person", t => t.SupervisorId)
                .Index(t => t.SupervisorId)
                .Index(t => t.ReviewerId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.student",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        Group = c.String(maxLength: 10, storeType: "nvarchar"),
                        EducationType = c.String(maxLength: 10, storeType: "nvarchar"),
                        InstituteId = c.String(maxLength: 36, storeType: "nvarchar"),
                        DepartamentId = c.String(maxLength: 36, storeType: "nvarchar"),
                        Email = c.String(maxLength: 255, storeType: "nvarchar"),
                        EducationStep = c.String(maxLength: 50, storeType: "nvarchar"),
                        EndYear = c.DateTime(precision: 0),
                        DativeFirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        DativeMiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        DativeLastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        GenitiveFirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        GenitiveMiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        GenitiveLastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        InstrumentalFirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        InstrumentalMiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        InstrumentalLastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        FirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        MiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        LastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        CreateDate = c.DateTime(precision: 0),
                        UpdateDate = c.DateTime(precision: 0),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.departament", t => t.DepartamentId)
                .ForeignKey("dbo.institute", t => t.InstituteId)
                .Index(t => t.InstituteId)
                .Index(t => t.DepartamentId);
            
            CreateTable(
                "dbo.departament",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        Name = c.String(maxLength: 255, storeType: "nvarchar"),
                        ShortName = c.String(maxLength: 50, storeType: "nvarchar"),
                        InstituteId = c.String(maxLength: 36, storeType: "nvarchar"),
                        CreateDate = c.DateTime(precision: 0),
                        UpdateDate = c.DateTime(precision: 0),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.institute", t => t.InstituteId)
                .Index(t => t.InstituteId);
            
            CreateTable(
                "dbo.institute",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        Name = c.String(nullable: false, maxLength: 255, storeType: "nvarchar"),
                        ShortName = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        CreateDate = c.DateTime(precision: 0),
                        UpdateDate = c.DateTime(precision: 0),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.gender",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        Name = c.String(maxLength: 255, storeType: "nvarchar"),
                        ShortName = c.String(maxLength: 50, storeType: "nvarchar"),
                        CreateDate = c.DateTime(precision: 0),
                        UpdateDate = c.DateTime(precision: 0),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.setting",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        key = c.String(unicode: false),
                        value = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.user",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        Login = c.String(maxLength: 50, storeType: "nvarchar"),
                        AccessDate = c.DateTime(precision: 0),
                        Password = c.String(maxLength: 50, storeType: "nvarchar"),
                        AccessLevel = c.Short(nullable: false),
                        FirstName = c.String(maxLength: 100, storeType: "nvarchar"),
                        MiddleName = c.String(maxLength: 100, storeType: "nvarchar"),
                        LastName = c.String(maxLength: 100, storeType: "nvarchar"),
                        CreateDate = c.DateTime(precision: 0),
                        UpdateDate = c.DateTime(precision: 0),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CommitteeMembers",
                c => new
                    {
                        CommitteeId = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                        CommitteeMemberId = c.String(nullable: false, maxLength: 36, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.CommitteeId, t.CommitteeMemberId })
                .ForeignKey("dbo.Committee", t => t.CommitteeId, cascadeDelete: true)
                .ForeignKey("dbo.CommitteeMember", t => t.CommitteeMemberId, cascadeDelete: true)
                .Index(t => t.CommitteeId)
                .Index(t => t.CommitteeMemberId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DefenseQuestion", "Defense_Id", "dbo.defense");
            DropForeignKey("dbo.defense", "GraduationQualificationWorkId", "dbo.GraduationQualificationWork");
            DropForeignKey("dbo.GraduationQualificationWork", "SupervisorId", "dbo.Person");
            DropForeignKey("dbo.GraduationQualificationWork", "StudentId", "dbo.student");
            DropForeignKey("dbo.student", "InstituteId", "dbo.institute");
            DropForeignKey("dbo.student", "DepartamentId", "dbo.departament");
            DropForeignKey("dbo.departament", "InstituteId", "dbo.institute");
            DropForeignKey("dbo.GraduationQualificationWork", "ReviewerId", "dbo.Person");
            DropForeignKey("dbo.defense", "CommitteeId", "dbo.Committee");
            DropForeignKey("dbo.DefenseQuestion", "CommitteeMemberId", "dbo.CommitteeMember");
            DropForeignKey("dbo.CommitteeMember", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Committee", "CommitteeSecretaryId", "dbo.CommitteeMember");
            DropForeignKey("dbo.CommitteeMembers", "CommitteeMemberId", "dbo.CommitteeMember");
            DropForeignKey("dbo.CommitteeMembers", "CommitteeId", "dbo.Committee");
            DropForeignKey("dbo.Committee", "CommitteeGeneralId", "dbo.CommitteeMember");
            DropIndex("dbo.CommitteeMembers", new[] { "CommitteeMemberId" });
            DropIndex("dbo.CommitteeMembers", new[] { "CommitteeId" });
            DropIndex("dbo.departament", new[] { "InstituteId" });
            DropIndex("dbo.student", new[] { "DepartamentId" });
            DropIndex("dbo.student", new[] { "InstituteId" });
            DropIndex("dbo.GraduationQualificationWork", new[] { "StudentId" });
            DropIndex("dbo.GraduationQualificationWork", new[] { "ReviewerId" });
            DropIndex("dbo.GraduationQualificationWork", new[] { "SupervisorId" });
            DropIndex("dbo.defense", new[] { "GraduationQualificationWorkId" });
            DropIndex("dbo.defense", new[] { "CommitteeId" });
            DropIndex("dbo.DefenseQuestion", new[] { "Defense_Id" });
            DropIndex("dbo.DefenseQuestion", new[] { "CommitteeMemberId" });
            DropIndex("dbo.Committee", new[] { "CommitteeSecretaryId" });
            DropIndex("dbo.Committee", new[] { "CommitteeGeneralId" });
            DropIndex("dbo.CommitteeMember", new[] { "PersonId" });
            DropTable("dbo.CommitteeMembers");
            DropTable("dbo.user");
            DropTable("dbo.setting");
            DropTable("dbo.gender");
            DropTable("dbo.institute");
            DropTable("dbo.departament");
            DropTable("dbo.student");
            DropTable("dbo.GraduationQualificationWork");
            DropTable("dbo.defense");
            DropTable("dbo.DefenseQuestion");
            DropTable("dbo.Person");
            DropTable("dbo.Committee");
            DropTable("dbo.CommitteeMember");
        }
    }
}

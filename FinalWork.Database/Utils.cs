﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using FinalWork.Model.Base;

namespace FinalWork.Database
{
    class Utils
    {
        public static Stream GetEmbeddedResourceStream(string resourceName)
        {
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
        }
        
        public static string[] GetEmbeddedResourceNames()
        {
            return Assembly.GetExecutingAssembly().GetManifestResourceNames();
        }

        public static string GetTextEmbeddedResource(string fileName)
        {
            string text = string.Empty;
            foreach (var embeddedResourceName in GetEmbeddedResourceNames())
            {
                if (embeddedResourceName.ToLowerInvariant().Contains(fileName.ToLowerInvariant()))
                {
                    var stream = GetEmbeddedResourceStream(embeddedResourceName);
                    var streamReader = new StreamReader(stream, Encoding.UTF8);
                    text = streamReader.ReadToEnd();
                    streamReader.Close();
                    stream.Close();
                    break;
                }
            }
            return text;
        }
    }

    public static class DbSetExtensions
    {
        public static T AddIfNotExists<T>(this DbSet<T> dbSet, T entity, Expression<Func<T, bool>> predicate = null) where T : class, new()
        {
            var exists = predicate != null ? dbSet.Any(predicate) : dbSet.Any();
            return !exists ? dbSet.Add(entity) : null;
        }

        public static T AddIfNotExists<T>(this DbSet<T> dbSet, T entity) where T : ModelBase, new()
        {
            var exists = dbSet.Any(d =>d.Id == entity.Id);
            return !exists ? dbSet.Add(entity) : null;
        }

        public static void AddIfNotExists<T>(this DbSet<T> dbSet, params T[] entities) where T : ModelBase, new()
        {
            foreach (var entity in entities)
            {
                AddIfNotExists(dbSet, entity);
            }
        }
    }
}

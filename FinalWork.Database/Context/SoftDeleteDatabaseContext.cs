﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using FinalWork.Database.Migrations;
using FinalWork.Model;
using FinalWork.Model.Interfaces;
using MySql.Data.MySqlClient;

namespace FinalWork.Database.Context
{
    public partial class DatabaseContext
        : DbContext
    {
        private static readonly Dictionary<Type, EntitySetBase> MappingCache =
            new Dictionary<Type, EntitySetBase>();

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            System.Data.Entity.Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<DatabaseContext, Configuration>());

            //сущности только помечаются как удаленные, поэтому нужно использовать фильтр по полю deleted
            modelBuilder.Entity<User>()
                .Map(m => m.Requires("Deleted").HasValue(false))
                .Ignore(m => m.Deleted);

            modelBuilder.Entity<Student>()
                .Map(m => m.Requires("Deleted").HasValue(false))
                .Ignore(m => m.Deleted);

            modelBuilder.Entity<Institute>()
                .Map(m => m.Requires("Deleted").HasValue(false))
                .Ignore(m => m.Deleted);

            modelBuilder.Entity<Departament>()
             .Map(m => m.Requires("Deleted").HasValue(false))
             .Ignore(m => m.Deleted);

            modelBuilder.Entity<Person>()
                .Map(m => m.Requires("Deleted").HasValue(false))
                .Ignore(m => m.Deleted);
            
            modelBuilder.Entity<Committee>()
                .HasMany(x => x.CommitteeMembers)
                .WithMany(x => x.Committies)
                .Map(x =>
                {
                    x.ToTable(nameof(Model.CommitteeMembers));
                    x.MapLeftKey(nameof(Model.CommitteeMembers.CommitteeId));
                    x.MapRightKey(nameof(Model.CommitteeMembers.CommitteeMemberId));
                });

            base.OnModelCreating(modelBuilder);
        }

        private void MarkAsDeleted()
        {
            //пометка сущностей как удаленные
            foreach (var entry in ChangeTracker.Entries()
                .Where(p => p.State == EntityState.Deleted && p.Entity is ISoftDeleted))
            {
                SoftDelete(entry);
            }
        }

        private void SoftDelete(DbEntityEntry entry)
        {
            var entryEntityType = entry.Entity.GetType();

            var tableName = GetTableName(entryEntityType);
            var primaryKeyName = GetPrimaryKeyName(entryEntityType);

            var sql = $"UPDATE {tableName.Substring(4)} SET deleted = 1 WHERE {primaryKeyName} = @id";

            Database.ExecuteSqlCommand(sql, new MySqlParameter("@id", entry.OriginalValues[primaryKeyName]) );

            // prevent hard delete
            entry.State = EntityState.Detached;
        }

        private string GetTableName(Type type)
        {
            var es = GetEntitySet(type);

            return $"{es.MetadataProperties["Schema"].Value}.{es.MetadataProperties["Table"].Value}";
        }

        private string GetPrimaryKeyName(Type type)
        {
            var es = GetEntitySet(type);

            return es.ElementType.KeyMembers[0].Name;
        }

        private EntitySetBase GetEntitySet(Type type)
        {
            if (!MappingCache.ContainsKey(type))
            {
                var octx = ((IObjectContextAdapter) this).ObjectContext;

                var typeName = ObjectContext.GetObjectType(type).Name;

                var es = octx.MetadataWorkspace
                    .GetItemCollection(DataSpace.SSpace)
                    .GetItems<EntityContainer>()
                    .SelectMany(c => c.BaseEntitySets
                        .Where(e => e.Name == typeName))
                    .FirstOrDefault();

                if (es == null)
                    throw new ArgumentException(@"Entity type not found in GetTableName", typeName);

                MappingCache.Add(type, es);
            }

            return MappingCache[type];
        }
    }
}
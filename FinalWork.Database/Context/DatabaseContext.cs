﻿using System.Data.Entity;
using System.Threading.Tasks;
using FinalWork.Model;
using MySql.Data.Entity;

namespace FinalWork.Database.Context
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public partial class DatabaseContext 
        : DbContext
    {
        public DatabaseContext(string connectionString) 
            : base(connectionString)
        {
            // http://stackoverflow.com/questions/14033193/entity-framework-provider-type-could-not-be-loaded
            var _ = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            
            Configuration.ProxyCreationEnabled = false;
        }
        public DatabaseContext() 
            : this("DefaultConnection")
        { }
        
        public DbSet<Student> Students { get; set; }
        public DbSet<Institute> Institutes { get; set; }
        public DbSet<Departament> Departaments { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Committee> Committis { get; set; }
        public DbSet<CommitteeMember> CommitteeMembers { get; set; }
        public DbSet<DefenseQuestion> DefenseQuestions { get; set; }
        public DbSet<Defense> Defenses { get; set; }
        public DbSet<Person> Person { get; set; }
        public DbSet<GraduationQualificationWork> GraduationQualificationWork { get; set; }

       // public DbSet<CommitteeMembers> CommitteeMembersLink { get; set; }


        public override int SaveChanges()
        {
            MarkAsDeleted();

            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync()
        {
            MarkAsDeleted();

            return await base.SaveChangesAsync();
        }
    }
}
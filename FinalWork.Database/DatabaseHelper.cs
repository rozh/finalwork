﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FinalWork.Database.Context;
using FinalWork.Database.Interfaces;
using FinalWork.Database.Repository;
using FinalWork.Model;

namespace FinalWork.Database
{
    /// <summary>
    ///     Помощник работы с БД
    /// </summary>
    public class DatabaseHelper : IDisposable
    {
        private readonly DatabaseContext _db;

        private readonly object _dbSaveLockObject = new object();
        private readonly Dictionary<Type, object> _repositoriesDictionary = new Dictionary<Type, object>();
        private readonly Dictionary<Type, Type> _modelToRepositoryDictionary = new Dictionary<Type, Type>();
        private bool _disposed;

        public DatabaseHelper()
        {
            _db = new DatabaseContext();
#if DEBUG
            _db.Database.Log += Log;
#endif
            // вызываем явную инициализацию контекста
            _db.Database.Initialize(false);

            Register<StudentRepository, Student>();
            Register<InstituteRepository, Institute>();
            Register<DepartamentRepository, Departament>();
            Register<SettingRepository, Setting>();
            Register<UserRepository, User>();
            Register<GenderRepository, Gender>();
            Register<PersonRepository, Person>();
        }
        
        /// <summary>
        ///     УНИЧТОЖИТЬ!!!!!!
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private static void Log(string s)
        {
            Debug.WriteLine(s);
        }

        /// <summary>
        ///     Регистрация нового репозитория
        /// </summary>
        private void Register<TRepository,TModel>()
        {
            var type = typeof(TRepository);
            if (!_repositoriesDictionary.ContainsKey(type))
            {
                _repositoriesDictionary.Add(type, null);
                _modelToRepositoryDictionary.Add(typeof(TModel), typeof(TRepository));
            }
        }

        /// <summary>
        ///     Получение репозитория
        /// </summary>
        /// <typeparam name="T">Тип репозитория</typeparam>
        /// <returns>Репозиторий</returns>
        public T GetRepo<T>()
        {
            return CreateAndChacheRepository<T>();
        }

        public object GetRepo(Type type)
        {
            return CreateAndChacheRepository(type);
        }

        private T CreateAndChacheRepository<T>()
        {
            var type = typeof(T);
            return (T)CreateAndChacheRepository(type);
        }

        private object CreateAndChacheRepository(Type type)
        {
            var repositoryType = type;
            if (_repositoriesDictionary.ContainsKey(repositoryType))
            {
                CheckAndCreateRepository(repositoryType);
            }
            else
            {
                if (_modelToRepositoryDictionary.ContainsKey(type))
                {
                    repositoryType = _modelToRepositoryDictionary[type];
                    CheckAndCreateRepository(repositoryType);
                }
            }

            return _repositoriesDictionary[repositoryType];
        }

        private void CheckAndCreateRepository(Type repositoryType)
        {
            if (_repositoriesDictionary[repositoryType] == null)
            {
                var instance = CreateRepository(repositoryType);
                _repositoriesDictionary[repositoryType] = instance;
            }
            else
            {
                throw new Exception($"Незарегистрированный тип {repositoryType}");
            }
        }

        private object CreateRepository(Type type)
        {
            if (_repositoriesDictionary.ContainsKey(type))
            {
                var ctor = type.GetConstructor(new[] {typeof(DatabaseContext)});
                object instance = null;
                if (ctor != null)
                {
                    instance = ctor.Invoke(new object[] {_db});
                }
                return instance;
            }
            throw new Exception($"Незарегистрированный тип {type}");
        }

        /// <summary>
        ///     Сохраняет изменения
        /// </summary>
        public void SaveChanges()
        {
            try
            {
                lock (_dbSaveLockObject)
                {
                    _db.SaveChanges();
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                throw HandleException(dbEx);
            }
        }

        /// <summary>
        ///     Перезагружает элементы в кэше
        /// </summary>
        public void Reload()
        {
            try
            {
                lock (_dbSaveLockObject)
                {
                    foreach (var rep in _repositoriesDictionary)
                    {
                        var grep = rep.Value as IGenericRepositorySave;
                        grep?.Reload();
                    }
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                throw HandleException(dbEx);
            }
        }
        
        /// <summary>
        ///     Сохраняет изменения
        /// </summary>
        public async Task<int> SaveChangesAsync()
        {
            try
            {
                return await Task<int>.Factory.StartNew(() =>
                {
                    var result = _db.SaveChangesAsync();
                    return result.Result;
                });
            }
            catch (DbEntityValidationException dbEx)
            {
                throw HandleException(dbEx);
            }
        }

        private static Exception HandleException(DbEntityValidationException dbEx)
        {
            var errorMessage =
                dbEx.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors)
                    .Aggregate(string.Empty,
                        (current, validationError) =>
                            current + Environment.NewLine + $"Property: {validationError.PropertyName} Error: {validationError.ErrorMessage}");

            return new Exception(errorMessage, dbEx);
        }

        /// <summary>
        ///     Очистка ресурсов
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
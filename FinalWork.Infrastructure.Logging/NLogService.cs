﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;

namespace FinalWork.Infrastructure.Logging
{
    /// <summary>
    /// Реализация сервиса логгирования на основании библиотеки Log4Net.
    /// </summary>
    public class NLogService
        : ILogService
    {
        /// <summary>
        /// Справочник логгировщиков для различных источников.
        /// </summary>
        private readonly Dictionary<string, Logger> _loggers = new Dictionary<string, Logger>();

        public NLogService()
        {
            //LogManager.

            // Получаем настройки из конфигурационного файла
            //var confSection = ConfigurationManager.GetSection("nlog") as XmlElement;
            //Logger.
            
            //log4net.Config.XmlConfigurator.Configure(confSection);
        }

        private Logger GetLog(string name)
        {
            lock (this)
            {
                if (_loggers.ContainsKey(name))
                {
                    return _loggers[name];
                }
                var logger = LogManager.GetLogger(name);
                _loggers.Add(name, logger);
                return logger;
            }
        }

        private Logger GetLog(string name, string userName)
        {
            lock (this)
            {
                if (_loggers.ContainsKey(name))
                {
                    return _loggers[name];
                }
                var logger = LogManager.GetLogger(name);
                _loggers.Add(name, logger);
                return logger;
            }
        }

        public ILogger GetLogger(string source)
        {
            return new NLogLogger(GetLog(source));
        }

        public ILogger GetLogger(Type sourceType)
        {
            return new NLogLogger(GetLog(sourceType.ToString()));
        }

        public ILogger GetLogger(Type sourceType, string instanceName)
        {
            var loggerName = string.Format("{0}[{1}]", sourceType.ToString(), instanceName);
            return new NLogLogger(GetLog(loggerName));
        }
    }
}
﻿using System;
using System.Globalization;

namespace FinalWork.Infrastructure.Logging
{
    public class SystemDebugLogger
        : ILogger
    {
        private readonly string _source;
        
        public SystemDebugLogger(string source)
        {
            _source = source;
        }

        public void Debug(string message)
        {
            System.Diagnostics.Debug.WriteLine("{0} DEBUG {1} - {2}", GetTimeString(), _source, message);
        }

        public void Info(string message)
        {
            System.Diagnostics.Debug.WriteLine("{0} INFO {1} - {2}", GetTimeString(), _source, message);
        }

        public void Warning(string message)
        {
            System.Diagnostics.Debug.WriteLine("{0} WARNING {1} - {2}", GetTimeString(), _source, message);
        }

        public void Error(string message)
        {
            System.Diagnostics.Debug.WriteLine("{0} ERROR {1} - {2}", GetTimeString(), _source, message);
        }

        public void Error(string message, Exception ex)
        {
            System.Diagnostics.Debug.WriteLine("{0} ERROR {1} - {4}\r\n{2}{3}", GetTimeString(), _source, ex.ToString(), ex.Message, message);
        }

        public void Error(Exception ex)
        {
            System.Diagnostics.Debug.WriteLine("{0} ERROR {1}\r\n{2}{3}", GetTimeString(), _source, ex.ToString(), ex.Message);
        }

        private static string GetTimeString()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
        }

        public void Trace(string message)
        {
            System.Diagnostics.Trace.WriteLine(string.Format("{0} DEBUG {1} - {2}", GetTimeString(), _source, message));
        }

        public void Fatal(string message, Exception ex)
        {
            this.Error(message, ex);
        }

        public void Fatal(Exception ex)
        {
            this.Error(ex);
        }

        public void Fatal(string message)
        {
            this.Error(message);
        }
    }
}
﻿using NLog;
using System;

namespace FinalWork.Infrastructure.Logging
{
    public class NLogLogger
        : ILogger
    {
        private readonly Logger _log;

        public NLogLogger(Logger log)
        {
            _log = log;
        }

        public void Trace(string message)
        {
            _log.Trace(message);
        }

        public void Debug(string message)
        {
            _log.Debug(message);
        }

        public void Info(string message)
        {
            _log.Info(message);
        }

        public void Warning(string message)
        {
            _log.Warn(message);
        }

        public void Error(string message)
        {
            _log.Error(message);
        }

        public void Error(string message, Exception ex)
        {
            _log.Error(ex, message);
        }

        public void Error(Exception ex)
        {
            _log.Error(ex, ex.Message);
        }

        public void Fatal(string message, Exception ex)
        {
            _log.Fatal(ex, message);
        }

        public void Fatal(Exception ex)
        {
            _log.Fatal(ex, ex.Message);
        }

        public void Fatal(string message)
        {
            _log.Fatal(message);
        }
    }
}
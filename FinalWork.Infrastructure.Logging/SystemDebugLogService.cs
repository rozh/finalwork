﻿using System;

namespace FinalWork.Infrastructure.Logging
{
    /// <summary>
    /// Реализация сервиса логгирования для отладочного вывода.
    /// Выводит все сообщения в отладчик VS.
    /// </summary>
    public class SystemDebugLogService
        : ILogService
    {
        public ILogger GetLogger(string source)
        {
            return new SystemDebugLogger(source);
        }

        public ILogger GetLogger(Type sourceType)
        {
            return new SystemDebugLogger(sourceType.ToString());
        }

        public ILogger GetLogger(Type sourceType, string instanceName)
        {
            var loggerName = string.Format("{0}[{1}]", sourceType.ToString(), instanceName);
            return new SystemDebugLogger(loggerName);
        }
    }
}
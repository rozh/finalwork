﻿using FinalWork.Infrastructure;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using FinalWork.Database;
using FinalWork.Database.Repository;
using FinalWork.UI.Utils;
using FinalWork.Windows.FatalErrorWindow;
using FinalWork.Windows.SplashScreen;

namespace FinalWork
{
    /// <summary>
    ///     Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly ILogger _logger;
        
        public App()
        {
            SplashScreenManager.Show("Загрузка...");
            _logger = ServiceLocator.Instance.LogService.GetLogger(GetType());

            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.ResourceResolve += AssemblyResolve;
            currentDomain.UnhandledException += ExceptionHandler;
            
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            Current.DispatcherUnhandledException += CurrentOnDispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;

            SplashScreenManager.SetMessage("Проверка повторного запуска...");
            ApplicationRunChecker.Check(Assembly.GetExecutingAssembly().GetName().Name, "FinalWork");

            SplashScreenManager.SetMessage("Проверка соединения с БД...");
            using (var dh = new DatabaseHelper())
            {
                dh.GetRepo<UserRepository>().Get();
            }

            SplashScreenManager.SetMessage("Загрузка главного окна...");
        }

        private void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs unobservedTaskExceptionEventArgs)
        {
            ExceptionHandler(sender, unobservedTaskExceptionEventArgs.Exception);
        }

        private void CurrentOnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs dispatcherUnhandledExceptionEventArgs)
        {
            ExceptionHandler(sender, dispatcherUnhandledExceptionEventArgs.Exception);
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            ExceptionHandler(sender, unhandledExceptionEventArgs.ExceptionObject);
        }
        
        private void ExceptionHandler(object sender, object exception)
        {
            var e = (Exception)exception;
            _logger.Fatal("Неожиданное исключение", e);
            _logger.Fatal(e.ToString());

            var innerExceptionStringBuilder = new StringBuilder(e.Message);
            var innerException = e.InnerException;
            while (innerException != null)
            {
                innerExceptionStringBuilder.Append("\r\n InnerException: ");
                innerExceptionStringBuilder.Append(innerException);
                innerException = innerException.InnerException;
            }

            string appName = AppDomain.CurrentDomain.FriendlyName;
            string appVer = "0.0.0.0";
            var assembly = Assembly.GetExecutingAssembly();
            if (assembly != null)
            {
                var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                appVer = fvi.FileVersion;
            }

            var reportErrorWindowVm = new FatalErrorWindowVm()
            {
                ExceptionMessage = innerExceptionStringBuilder.ToString(),
                CallStack = e.StackTrace,
                App = appName + " [" + appVer + "]",
                DateTime = DateTime.Now,
                Message = ""
            };
            var errorWindow = new FatalErrorWindow {DataContext = reportErrorWindowVm};
            errorWindow.ShowDialog();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            
        }
        
        private static Assembly AssemblyResolve(object sender, ResolveEventArgs args)
        {
            Console.WriteLine(Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).FullName).FullName);

            string assemblyPath = Path.Combine(
                Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).FullName).FullName,
                new AssemblyName(args.Name).Name + ".dll");

            if (File.Exists(assemblyPath))
            {
                return Assembly.LoadFrom(assemblyPath);
            }
            return null;
        }
    }
}
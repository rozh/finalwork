﻿using FinalWork.ViewModel;

namespace FinalWork.UI.Workspaces
{
    /// <summary>
    ///     Добавляет поддержку пользователя, проверки уровня доступа и вывода информации
    ///     в конец заголовка окна, при наличии высоких привелегий (прав доступа) или их отсутствии
    /// </summary>
    public abstract class LoginSupportedWorkspace : WorkspaceBase
    {
        protected string _title;
        private UserVm _user;

        /// <summary>Заголовок (описание сущности)</summary>
        public virtual string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged();
            }
        }

        public UserVm CurrentUser
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged();
            }
        }
    }
}
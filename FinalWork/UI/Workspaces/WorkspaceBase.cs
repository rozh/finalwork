﻿using System;
using FinalWork.Infrastructure;
using FinalWork.UI.Utils.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace FinalWork.UI.Workspaces
{
    public class WorkspaceBase : ViewModelBase
    {
        private bool _dialogResut;
        private ILogger _logger;
        private RelayCommand<object> _requestClosingCommand;
        private RelayCommand<object> _requestHideCommand;

        /// <summary>
        ///     Логировщик. Позволяет записывать сообщения в лог
        /// </summary>
        protected ILogger Logger => _logger ?? (_logger = ServiceLocator.Instance.LogService.GetLogger(GetType()));

        /// <summary>
        ///     Результат закрытия окна как окна закрытого после вызова ShowDialog
        /// </summary>
        public bool DialogResult
        {
            get { return _dialogResut; }
            set
            {
                _dialogResut = value;
                RaisePropertyChanged();
            }
        }

        public object Tag { get; set; }

        ///// <summary>
        ///// Возвращает или задаёт успешен ли результат выполнения операции. Аналогично DialogResult, но не
        ///// должен зависеть от того с каким результатом было закрыто окно. Это результат пользователь должен 
        ///// назначать вручную после выполнения какой-либо операции и закрытии окна.
        ///// </summary>
        //public bool OperationSuccess { get; set; }

        /// <summary>
        ///     Это свойство позволяет работать с общим контейнером окон (<see cref="WindowsContainer" />)
        ///     для любого контрола, если контрол находится, к примеру, в отдельном проекте, то ему как-то нужно
        ///     получать доступ к контейнеру, чтобы этот контрол, помещённый в окно  мог получить доступ к родителю,
        ///     то есть к окну создавшему его. Или, если окно находится в отдельном проекте, то ему нужно
        ///     как-то уметь добавить себя в качестве дочернего окна родительскому окну из основного проекта.
        ///     Сам контейнер внутри себя каждой вью модели наследующей от этого класса присваивает ссылку на себя, тем самым это
        ///     свойство
        ///     никогда не остаётся пустым, если для этой вью модели открывали окно. В общем, офигенная идея!
        /// </summary>
        public WindowsContainer WindowsContainer { get; set; }

        /// <summary>
        ///     Возвращает или задаёт открыто ли представление для данной вью модели
        /// </summary>
        public bool IsOpened { get; set; }

        /// <summary>
        ///     Инициирует срабатывание событие CloseRequested
        /// </summary>
        public virtual RelayCommand<object> RequestClosingCommand
        {
            get
            {
                return _requestClosingCommand ?? (_requestClosingCommand = new RelayCommand<object>(
                    param => OnCloseRequested()));
            }
        }

        public virtual RelayCommand<object> RequestHideCommand
        {
            get
            {
                return _requestHideCommand ?? (_requestHideCommand = new RelayCommand<object>(
                    param => OnHideRequested()));
            }
        }

        /// <summary>
        ///     Возникает перед тем, как View будет закрыто.
        /// </summary>
        public event EventHandler PreviewCloseRequested;

        /// <summary>
        ///     Не следует использовать это событие в пользовательском коде. Зарезервировано для View контейнеров для закрытия
        ///     окна.
        ///     Перед тем как завершится обработка этого события будет инициировано событие <see cref="PreviewCloseRequested" />
        /// </summary>
        public event EventHandler CloseRequested;

        public event EventHandler HideRequested;

        /// <summary>Инициировать запрос на скрытие окна</summary>
        protected virtual void OnHideRequested()
        {
            var handler = HideRequested;
            handler?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>Инициировать запрос на закрытие окна</summary>
        internal void OnPreviewCloseRequested()
        {
            var handler = PreviewCloseRequested;
            handler?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>Инициировать запрос на закрытие окна</summary>
        protected virtual void OnCloseRequested()
        {
            var handler = CloseRequested;
            handler?.Invoke(this, EventArgs.Empty);
        }
    }
}
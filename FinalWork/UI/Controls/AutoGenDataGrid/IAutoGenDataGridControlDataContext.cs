﻿using System;
using System.Collections.Generic;

namespace FinalWork.UI.Controls.AutoGenDataGrid
{
    interface IAutoGenDataGridControlDataContext
    {
        Type GetViewModelType();

        Dictionary<string, List<object>> ComboBoxData { get; }
    }
}

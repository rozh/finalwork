﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace FinalWork.UI.Converters
{
    [ValueConversion(typeof (TimeSpan), typeof (string))]
    public class TimeSpanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var timeSpan = (TimeSpan) value;
            var days = timeSpan.Days;
            var hours = timeSpan.Hours;
            var minutes = timeSpan.Minutes;
            var seconds = timeSpan.Seconds;

            var format = parameter.ToString().ToLowerInvariant();

            var parts = new List<string>();
            if (format.IndexOf('d') != -1 && days != 0) parts.Add($"{days} д");
            if (format.IndexOf('h') != -1 && hours != 0) parts.Add($"{hours:00} ч");
            if (format.IndexOf('m') != -1 && minutes != 0) parts.Add($"{minutes:00} м");
            if (format.IndexOf('s') != -1 && seconds != 0) parts.Add($"{seconds:00} с");

            return string.Join(" ", parts);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
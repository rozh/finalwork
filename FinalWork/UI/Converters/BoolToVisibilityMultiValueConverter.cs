﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace FinalWork.UI.Converters
{
    public class BoolToVisibilityMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool result = false;

            foreach (var value in values)
            {
                if (value is bool)
                {
                    var val = (bool) value;
                    result |= val;
                }
            }

            return result ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using FinalWork.Utils.TypeExtensions;

namespace FinalWork.UI.Converters
{
    public class CurrentPageComparer : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string parameterString = parameter as string;
            string pageKey = value as string;
            if (!string.IsNullOrEmpty(parameterString) && !string.IsNullOrEmpty(pageKey))
            {
                string[] parameters = parameterString.Split(new char[] { '|' });
                pageKey = pageKey.ToLowerInvariant();
                return parameters.Any(p => p.ToLowerInvariant().Is(pageKey));
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
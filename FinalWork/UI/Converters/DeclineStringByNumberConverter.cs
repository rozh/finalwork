﻿using System;
using System.Globalization;
using System.Windows.Data;
using FinalWork.Utils;

namespace FinalWork.UI.Converters
{
    public class DeclineStringByNumberConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string parameterString = parameter as string;
            if (!string.IsNullOrEmpty(parameterString))
            {
                string[] parameters = parameterString.Split(new char[] { '|' });
                if (value != null && parameters.Length>2)
                {
                    var num = (int) value;
                    return NumericDeclensionHelper.Decline(num, parameters[0], parameters[1], parameters[2]);
                }
                return parameters[0];
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
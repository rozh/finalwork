﻿using System;
using System.Globalization;
using System.Windows.Data;
using FinalWork.Utils.TypeExtensions;

namespace FinalWork.UI.Converters
{
    [ValueConversion(typeof(Enum), typeof(string))]
    public class EnumDisplayNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var @enum = value as Enum;
            if (@enum == null) return Binding.DoNothing;

            var enumValue = @enum;
            return enumValue.GetDisplayName();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
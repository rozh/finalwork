﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace FinalWork.UI.Converters
{
    public class StringCaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = value as string;

            if (val == null) return value;
            if (parameter == null) return val;
            
            var casing = (CharacterCasing) parameter;
            switch (casing)
            {
                case CharacterCasing.Lower:
                    return val?.ToLower(culture);
                case CharacterCasing.Upper:
                    return val?.ToUpper(culture);
                default:
                    return val;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
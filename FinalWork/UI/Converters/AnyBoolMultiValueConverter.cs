﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace FinalWork.UI.Converters
{
    public class AnyBoolMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool result = false;

            if (!values.All(v => v is bool)) return Binding.DoNothing;

            foreach (var value in values)
            {
                var val = (bool) value;
                result |= val;
            }

            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

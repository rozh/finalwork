﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace FinalWork.UI.Converters
{
    [ValueConversion(typeof (SolidColorBrush), typeof (Color))]
    public class SolidColorBrushToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SolidColorBrush)
            {
                var brush = value as SolidColorBrush;
                return brush.Color;
            }

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Color)
            {
                var color = (Color) value;
                return new SolidColorBrush(color);
            }

            return Binding.DoNothing;
        }
    }
}
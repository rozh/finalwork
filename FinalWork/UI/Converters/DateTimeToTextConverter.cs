﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FinalWork.UI.Converters
{
    [ValueConversion(typeof (bool), typeof (bool))]
    public class DateTimeToTextConverter : IValueConverter
    {
        private string _format = "dd.MM.yyyy HH:mm";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime)
            {
                _format = parameter?.ToString() ?? _format;

                var date = (DateTime) value;

                return date.ToString(_format);
            }
            if (value is string)
            {
                return ConvertBack(value, targetType, parameter, culture);
            }

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = value as string;
            if (s != null)
            {
                var strDate = s;
                try
                {
                    var date = DateTime.ParseExact(strDate, _format, null);
                    return date;
                }
                catch
                {
                    // ignored
                }
            }
            else if (value is DateTime)
            {
                return Convert(value, targetType, parameter, culture);
            }

            return Binding.DoNothing;
        }
    }
}
﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FinalWork.UI.Converters
{
    [ValueConversion(typeof(string), typeof(bool))]
    public class StringLengthNotZeroConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var @string = value?.ToString();

            return !string.IsNullOrWhiteSpace(@string);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
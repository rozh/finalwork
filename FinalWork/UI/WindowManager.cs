﻿using FinalWork.UI.Utils.Windows;

namespace FinalWork.UI
{
    public class WindowManager : WindowManagerBase
    {
        static WindowManager()
        {
            Instance = new WindowManager();
        }

        /// <summary>
        ///     Возвращает единственный экземпляр объекта в памяти.
        /// </summary>
        public static WindowManager Instance { get; }
    }
}
﻿using System;
using System.Threading;
using System.Windows;
using FinalWork.Infrastructure;
using FinalWork.Utils.WinApi;
using FinalWork.Utils.WinApi._Enums;

namespace FinalWork.UI.Utils
{
    public static class ApplicationRunChecker
    {
        static private Mutex _mutex;
        public static uint BringToFrontMessage;

        public static void Check(string id, string programName)
        {
            BringToFrontMessage = User32.RegisterWindowMessage(id);
            string mutexName = "Global\\" + id;

            _mutex = new Mutex(false, mutexName);

            if (!_mutex.WaitOne(0, false))
            {
                MessageBox.Show("Программа уже запущена", programName, MessageBoxButton.OK,
                    MessageBoxImage.Hand);

                User32.PostMessage(
                    (IntPtr)Other.HWND_BROADCAST,
                    BringToFrontMessage,
                    IntPtr.Zero,
                    IntPtr.Zero);

                Environment.Exit(-1);
            }

            GC.KeepAlive(_mutex);
        }
    }
}

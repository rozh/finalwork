﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using FinalWork.Model;
using FinalWork.UI.Workspaces;
using FinalWork.ViewModel;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;

namespace FinalWork.UI.Utils
{
    public class FrameNavigationService 
        : INotifyPropertyChanged
        , INavigationService
    {
        private readonly Dictionary<string, PageConfig> _pagesByKey;
        private readonly List<string> _historic;
        private string _currentPageKey;
                                       
        public string CurrentPageKey
        {
            get
            {
                return _currentPageKey;
            }

            private set
            {
                if (_currentPageKey == value)
                {
                    return;
                }

                _currentPageKey = value;
                OnPropertyChanged("CurrentPageKey");
            }
        }
        public object Parameter { get; private set; }

        public User CurrentUser { get; set; }

        public FrameNavigationService()
        {
            _pagesByKey = new Dictionary<string, PageConfig>();
            _historic = new List<string>();
        }
        public void GoBack()
        {
            if (_historic.Count > 1)
            {
                _historic.RemoveAt(_historic.Count - 1);
                NavigateTo(_historic.Last(), null);
            }
        }
        public void NavigateTo(string pageKey)
        {
            NavigateTo(pageKey, null);
        }

        public virtual void NavigateTo(string pageKey, object parameter)
        {
            lock (_pagesByKey)
            {
                if (!_pagesByKey.ContainsKey(pageKey))
                {
                    throw new ArgumentException(string.Format("No such page: {0} ", pageKey), "pageKey");
                }

                Parameter = parameter;
                var frame = GetDescendantFromName(Application.Current.MainWindow, "MainFrame") as Frame;

                if (frame != null)
                {
                    var pc = _pagesByKey[pageKey];
                    frame.Source = pc.Uri;
                    frame.DataContext = pc.DataContext;
                    var page = pc.DataContext as INavigationPage;
                    if (page != null)
                    {
                        if (parameter != null)
                            page.SetParams(parameter);
                        page.Navigated();
                    }

                    var userPage = pc.DataContext as LoginSupportedWorkspace;
                    if (userPage != null)
                    {
                        userPage.CurrentUser = new UserVm(CurrentUser);
                    }
                }
                _historic.Add(pageKey);
                CurrentPageKey = pageKey;
            }
        }

        public void Configure(string key, Uri pageType, Type dataContextType)
        {
            lock (_pagesByKey)
            {
                var pc = new PageConfig() { Uri = pageType, DataContextType = dataContextType };
                if (_pagesByKey.ContainsKey(key))
                {
                    _pagesByKey[key] = pc;
                }
                else
                {
                    _pagesByKey.Add(key, pc);
                }
            }
        }

        public void Configure(string key, Uri pageType, object dataContext = null)
        {
            lock (_pagesByKey)
            {
                var pc = new PageConfig() {Uri = pageType, DataContext = dataContext};
                if (_pagesByKey.ContainsKey(key))
                {
                    _pagesByKey[key] = pc;
                }
                else
                {
                    _pagesByKey.Add(key, pc);
                }
            }
        }

        private static FrameworkElement GetDescendantFromName(DependencyObject parent, string name)
        {
            var count = VisualTreeHelper.GetChildrenCount(parent);

            if (count < 1)
            {
                return null;
            }

            for (var i = 0; i < count; i++)
            {
                var frameworkElement = VisualTreeHelper.GetChild(parent, i) as FrameworkElement;
                if (frameworkElement != null)
                {
                    if (frameworkElement.Name == name)
                    {
                        return frameworkElement;
                    }

                    frameworkElement = GetDescendantFromName(frameworkElement, name);
                    if (frameworkElement != null)
                    {
                        return frameworkElement;
                    }
                }
            }
            return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    class PageConfig
    {
        private object _dataContext;
        public Uri Uri { get; set; }

        public object DataContext
        {
            get { return DataContextType == null ? _dataContext : ServiceLocator.Current.GetInstance(DataContextType); }
            set { _dataContext = value; }
        }
        public Type DataContextType { get; set; }
    }
}

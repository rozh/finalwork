﻿using System.Windows;
using System.Xml.Serialization;

namespace FinalWork.UI.Utils.Windows.Configuration
{
    public class SerializableSize
    {
        public SerializableSize()
            : this(0d, 0d)
        {
        }

        public SerializableSize(double width, double height)
        {
            Width = width;
            Height = height;
        }

        /// <summary>Ширина</summary>
        [XmlAttribute]
        public double Width { get; set; }

        /// <summary>Высота</summary>
        [XmlAttribute]
        public double Height { get; set; }

        /// <summary>
        ///     Неявное преобразование типа
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static implicit operator Size(SerializableSize size)
        {
            return new Size(size.Width, size.Height);
        }

        /// <summary>
        ///     Неявное преобразование типа
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static implicit operator SerializableSize(Size size)
        {
            return new SerializableSize(size.Width, size.Height);
        }
    }

    public class SerializablePoint
    {
        public SerializablePoint()
            : this(0d, 0d)
        {
        }

        public SerializablePoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>Ширина</summary>
        [XmlAttribute]
        public double X { get; set; }

        /// <summary>Высота</summary>
        [XmlAttribute]
        public double Y { get; set; }

        /// <summary>
        ///     Неявное преобразование типа
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static implicit operator Point(SerializablePoint point)
        {
            return new Point(point.X, point.Y);
        }

        /// <summary>
        ///     Неявное преобразование типа
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static implicit operator SerializablePoint(Point point)
        {
            return new SerializablePoint(point.X, point.Y);
        }
    }

    /// <summary>
    ///     Настройки окна
    /// </summary>
    public class WindowSettings
    {
        public WindowSettings()
        {
            Size = new SerializableSize();
            Position = new SerializablePoint(-1d, -1d);
            WindowState = WindowState.Maximized;
        }

        /// <summary>Размер окна</summary>
        [XmlElement(ElementName = "Size")]
        public SerializableSize Size { get; set; }

        /// <summary>Позиция окна</summary>
        [XmlElement(ElementName = "Position")]
        public SerializablePoint Position { get; set; }

        /// <summary>
        ///     Состояние окна.
        /// </summary>
        [XmlAttribute("WindowState")]
        public WindowState WindowState { get; set; }


        [XmlIgnore]
        public bool IsDefault
        {
            get { return Size == new Size() && Position == new Point(-1d, -1d); }
        }
    }
}
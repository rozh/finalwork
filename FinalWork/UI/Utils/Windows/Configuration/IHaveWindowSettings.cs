﻿namespace FinalWork.UI.Utils.Windows.Configuration
{
    public interface IHaveWindowSettings
    {
        /// <summary>
        ///     Настройки окна
        /// </summary>
        WindowSettings WindowSettings { get; set; }
    }
}
﻿using System.Windows;
using GalaSoft.MvvmLight;

namespace FinalWork.UI.Utils.Windows
{
    public abstract class WindowManagerBase
    {
        public WindowsContainer WindowsContainer { get; protected set; }

        public void Initialize(Window mainWindow, ViewModelBase viewModel)
        {
            WindowsContainer = new WindowsContainer(mainWindow, viewModel);
        }
    }
}
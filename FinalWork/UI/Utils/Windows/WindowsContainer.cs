﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;
using FinalWork.UI.Workspaces;
using GalaSoft.MvvmLight;

namespace FinalWork.UI.Utils.Windows
{
    public class WindowsContainer
    {
        private readonly Dictionary<ViewModelBase, Window> _windows;

        public WindowsContainer(Window mainWindow, ViewModelBase mainViewModel)
        {
            _windows = new Dictionary<ViewModelBase, Window>();

            mainWindow.Closed += Window_Closed;
            var @base = mainViewModel as WorkspaceBase;
            if (@base != null)
            {
                @base.WindowsContainer = this;
            }

            _windows.Add(mainViewModel, mainWindow);
        }

        public bool WindowOpened<TViewModel>(TViewModel viewModel)
            where TViewModel : WorkspaceBase
        {
            if (viewModel == null || !_windows.ContainsKey(viewModel)) return false;

            var window = _windows[viewModel];
            return window.Visibility == Visibility.Visible;
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <param name="viewModel"></param>
        /// <param name="owner"></param>
        /// <param name="considerTaskBar">
        ///     Задаёт будет ли окно учитывать размер и положение панели задач при разворачивании окна во
        ///     весь экран
        /// </param>
        /// <returns></returns>
        public WorkspaceBase OpenDialog<TWindow>(WorkspaceBase viewModel, ViewModelBase owner, bool considerTaskBar)
            where TWindow : WindowBase
        {
            return OpenWindow<TWindow>(viewModel, owner, true, considerTaskBar);
        }

        public WorkspaceBase OpenDialog<TWindow>(WorkspaceBase viewModel, ViewModelBase owner)
            where TWindow : Window
        {
            return OpenWindow<TWindow>(viewModel, owner, true);
        }

        /// <summary>
        ///     Создаёт окно, кладёт его в словарь, ключом является ViewModel. Если в качестве основной вью модели передать null,
        ///     то будет создано окно и получена вью модель из его свойства DataContext.
        /// </summary>
        /// <typeparam name="TWindow">Тип создаваемого окна</typeparam>
        /// <param name="viewModel"></param>
        /// <param name="owner"></param>
        /// <param name="asDialog">Задаёт открывать ли окно как диалоговое</param>
        /// <param name="considerTaskBar">
        ///     Задаёт будет ли окно учитывать размер и положение панели задач при разворачивании окна во
        ///     весь экран
        /// </param>
        public WorkspaceBase OpenWindow<TWindow>(WorkspaceBase viewModel, ViewModelBase owner, bool asDialog = false,
            bool? considerTaskBar = null)
            where TWindow : Window
        {
            if (viewModel != null && _windows.ContainsKey(viewModel))
            {
                var window = _windows[viewModel];

                if (_windows.ContainsKey(owner))
                {
                    var vm = _windows[owner].DataContext as ViewModelBase;
                    TryUpdateCurrentUser(viewModel, vm);
                }

                viewModel.IsOpened = true;

                var windowBase = window as WindowBase;
                if (windowBase != null)
                {
                    if (window.WindowState == WindowState.Minimized)
                        window.WindowState = windowBase.PrevState ?? WindowState.Normal;
                }
                else
                {
                    if (window.WindowState == WindowState.Minimized)
                        window.WindowState = WindowState.Normal;
                }

                window.Show();
                window.Activate();
            }
            else
            {
                var dialog = (TWindow) typeof (TWindow).GetConstructors()[0].Invoke(null);
                if (considerTaskBar != null)
                {
                    var windowBase = dialog as WindowBase;
                    if (windowBase != null)
                    {
                        windowBase.ConsiderTheSizeOfTheTaskbar = considerTaskBar.Value;
                    }
                }
                if (viewModel == null)
                {
                    if (dialog.DataContext == null)
                    {
                        Debug.Fail("Попытка создать окно с пустым DataContext не передав ViewModel для создания окна");
                    }
                    viewModel = (WorkspaceBase) dialog.DataContext;
                }
                else
                {
                    dialog.DataContext = viewModel;
                }

                dialog.Closed += Window_Closed;
                viewModel.CloseRequested += ViewModel_CloseRequested;
                viewModel.HideRequested += ViewModel_HideRequested;
                viewModel.WindowsContainer = this;

                _windows.Add(viewModel, dialog);

                if (_windows.ContainsKey(owner))
                {
                    dialog.Owner = _windows[owner];
                    var vm = _windows[owner].DataContext as ViewModelBase;
                    TryUpdateCurrentUser(viewModel, vm);
                }

                viewModel.IsOpened = true;
                if (asDialog)
                {
                    dialog.ShowDialog();
                }
                else
                {
                    dialog.Show();
                }
            }

            return viewModel;
        }

        private static void TryUpdateCurrentUser(WorkspaceBase viewModel, ViewModelBase vm)
        {
            if (vm is LoginSupportedWorkspace && viewModel is LoginSupportedWorkspace)
            {
                var loginWorkspace = (LoginSupportedWorkspace) vm;
                var vmLogin = (LoginSupportedWorkspace) viewModel;
                vmLogin.CurrentUser = loginWorkspace.CurrentUser;
            }
        }

        private void ViewModel_HideRequested(object sender, EventArgs e)
        {
            var workspace = (WorkspaceBase) sender;
            workspace.IsOpened = false;
            _windows[workspace].Hide();
        }

        private void ViewModel_CloseRequested(object sender, EventArgs e)
        {
            var vm = (WorkspaceBase) sender;
            vm.OnPreviewCloseRequested();

            if (_windows.ContainsKey(vm))
            {
                _windows[vm].Close();
            }
            else
            {
                Debug.Fail("Не удалось найти объект класса модели представления в словаре");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            var window = (Window) sender;
            window.Closed -= Window_Closed;

            var vm = window.DataContext as ViewModelBase;
            if (vm != null)
            {
                if (vm is WorkspaceBase)
                {
                    var workspace = (WorkspaceBase) vm;
                    workspace.IsOpened = false;
                    workspace.HideRequested -= ViewModel_HideRequested;
                    workspace.CloseRequested -= ViewModel_CloseRequested;
                }

                if (_windows.ContainsKey(vm))
                {
                    _windows.Remove(vm);
                }
            }

            if (window.Owner != null)
            {
                window.Owner.Activate();
                window.Owner.Focus();
            }

            BindingOperations.ClearBinding(window, FrameworkElement.DataContextProperty);
            GC.Collect();
        }
    }
}
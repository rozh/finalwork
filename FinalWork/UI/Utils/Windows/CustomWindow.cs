﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Shapes;

namespace FinalWork.UI.Utils.Windows
{
    internal enum ResizeDirection
    {
        Left = 1,
        Right = 2,
        Top = 3,
        TopLeft = 4,
        TopRight = 5,
        Bottom = 6,
        BottomLeft = 7,
        BottomRight = 8
    }

    public enum ControlBoxVisibility
    {
        /// <summary>
        ///     Состояние по умолчанию, все кнопки видны
        /// </summary>
        Default,

        /// <summary>
        ///     Кнопка "Свернуть" скрыта
        /// </summary>
        MinimizeButtonCollapsed,

        /// <summary>
        ///     Все кнопки скрыты
        /// </summary>
        AllButtonsCollapsed
    }

    public class CustomWindow : WindowBase
    {
        /// <summary>
        ///     A Window receives this message when the user chooses a command from the Window menu
        ///     (formerly known as the system or control menu) or when the user chooses the maximize
        ///     button minimize button restore button or close button.
        /// </summary>
        public const int WM_SYSCOMMAND = 0x112;

        public static readonly DependencyProperty ControlBoxVisibilityProperty = DependencyProperty.Register(
            "ControlBoxVisibility", typeof (ControlBoxVisibility), typeof (CustomWindow),
            new PropertyMetadata(default(ControlBoxVisibility)));

        private HwndSource _hwndSource;


        static CustomWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof (CustomWindow),
                new FrameworkPropertyMetadata(typeof (CustomWindow)));
        }

        public CustomWindow()
        {
            PreviewMouseMove += OnPreviewMouseMove;
        }

        public ControlBoxVisibility ControlBoxVisibility
        {
            get { return (ControlBoxVisibility) GetValue(ControlBoxVisibilityProperty); }
            set { SetValue(ControlBoxVisibilityProperty, value); }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        protected override void OnInitialized(EventArgs e)
        {
            SourceInitialized += OnSourceInitialized;
            base.OnInitialized(e);
        }

        private void OnSourceInitialized(object sender, EventArgs e)
        {
            _hwndSource = (HwndSource) PresentationSource.FromVisual(this);
        }

        protected void ResizeRectangle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var rectangle = sender as Rectangle;
            Debug.Assert(rectangle != null, "rectangle == null");
            switch (rectangle.Name)
            {
                case "top":
                    Cursor = Cursors.SizeNS;
                    ResizeWindow(ResizeDirection.Top);
                    break;
                case "bottom":
                    Cursor = Cursors.SizeNS;
                    ResizeWindow(ResizeDirection.Bottom);
                    break;
                case "left":
                    Cursor = Cursors.SizeWE;
                    ResizeWindow(ResizeDirection.Left);
                    break;
                case "right":
                    Cursor = Cursors.SizeWE;
                    ResizeWindow(ResizeDirection.Right);
                    break;
                case "topLeft":
                    Cursor = Cursors.SizeNWSE;
                    ResizeWindow(ResizeDirection.TopLeft);
                    break;
                case "topRight":
                    Cursor = Cursors.SizeNESW;
                    ResizeWindow(ResizeDirection.TopRight);
                    break;
                case "bottomLeft":
                    Cursor = Cursors.SizeNESW;
                    ResizeWindow(ResizeDirection.BottomLeft);
                    break;
                case "bottomRight":
                    Cursor = Cursors.SizeNWSE;
                    ResizeWindow(ResizeDirection.BottomRight);
                    break;
                default:
                    break;
            }
        }

        private void ResizeWindow(ResizeDirection direction)
        {
            SendMessage(_hwndSource.Handle, WM_SYSCOMMAND, (IntPtr) (61440 + direction), IntPtr.Zero);
        }

        protected void OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton != MouseButtonState.Pressed)
                Cursor = Cursors.Arrow;
        }

        public override void OnApplyTemplate()
        {
            var minimizeButton = GetTemplateChild("minimizeButton") as Button;
            if (minimizeButton != null)
                minimizeButton.Click += MinimizeClick;

            var restoreButton = GetTemplateChild("restoreButton") as Button;
            if (restoreButton != null)
                restoreButton.Click += RestoreClick;

            var closeButton = GetTemplateChild("closeButton") as Button;
            if (closeButton != null)
                closeButton.Click += CloseClick;

            var resizeGrid = GetTemplateChild("resizeGrid") as Grid;
            if (resizeGrid != null)
            {
                foreach (UIElement element in resizeGrid.Children)
                {
                    var resizeRectangle = element as Rectangle;
                    if (resizeRectangle != null)
                    {
                        resizeRectangle.PreviewMouseDown += ResizeRectangle_PreviewMouseDown;
                        resizeRectangle.MouseMove += ResizeRectangle_MouseMove;
                    }
                }
            }

            var rect = GetTemplateChild("moveRectangle") as Rectangle;
            if (rect != null)
            {
                rect.PreviewMouseLeftButtonDown += MoveRectangle_PreviewMouseDown;
            }
            var title = GetTemplateChild("titleTextBlock") as TextBlock;
            if (title != null)
            {
                title.PreviewMouseLeftButtonDown += MoveRectangle_PreviewMouseDown;
            }

            base.OnApplyTemplate();
        }

        private void MoveRectangle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                RestoreClick(sender, e);
                return;
            }

            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        protected void ResizeRectangle_MouseMove(object sender, MouseEventArgs e)
        {
            var rectangle = sender as Rectangle;
            switch (rectangle.Name)
            {
                case "top":
                    Cursor = Cursors.SizeNS;
                    break;
                case "bottom":
                    Cursor = Cursors.SizeNS;
                    break;
                case "left":
                    Cursor = Cursors.SizeWE;
                    break;
                case "right":
                    Cursor = Cursors.SizeWE;
                    break;
                case "topLeft":
                    Cursor = Cursors.SizeNWSE;
                    break;
                case "topRight":
                    Cursor = Cursors.SizeNESW;
                    break;
                case "bottomLeft":
                    Cursor = Cursors.SizeNESW;
                    break;
                case "bottomRight":
                    Cursor = Cursors.SizeNWSE;
                    break;
                default:
                    break;
            }
        }

        #region Click events

        protected void MinimizeClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        protected void RestoreClick(object sender, RoutedEventArgs e)
        {
            if (ResizeMode == ResizeMode.NoResize)
            {
                return;
            }
            WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
        }

        protected void CloseClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion
    }
}
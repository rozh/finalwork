﻿namespace FinalWork.UI.Utils
{
    public interface INavigationPage
    {
        void SetParams(object parameter);
        void Navigated();
    }
}

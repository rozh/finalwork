﻿using System;

namespace FinalWork.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    class IncludePropsToLoadAttribute
        : Attribute
    {
        public string[] Include { get; }

        public IncludePropsToLoadAttribute(params string[] include)
        {
            Include = include;
        }
    }
}

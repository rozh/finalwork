﻿using System;

namespace FinalWork.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    class ComboBoxAttribute 
        : Attribute
    {
        public string SelectedValueProperty { get; }
        public string DisplayMemberPath { get; }
        public string SelectedValuePath { get; }
        public Type ModelType { get; }

        public ComboBoxAttribute(string selectedValueProperty, string displayMemberPath, string selectedValuePath, Type modelType)
        {
            SelectedValueProperty = selectedValueProperty;
            DisplayMemberPath = displayMemberPath;
            SelectedValuePath = selectedValuePath;
            ModelType = modelType;
        }
    }
}

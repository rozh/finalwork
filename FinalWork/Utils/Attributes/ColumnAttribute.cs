﻿using System;

namespace FinalWork.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    class ColumnAttribute
        : Attribute
    {
        public int OrderNumber { get; }
        public string Name { get; }

        public ColumnAttribute(int orderNumber, string name)
        {
            OrderNumber = orderNumber;
            Name = name;
        }
    }
}

﻿using System;

namespace FinalWork.Utils.TypeExtensions
{
    public static class StringExtensions
    {
        public static bool Is(this string @string, string compareTo)
        {
            return @string.Equals(compareTo, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
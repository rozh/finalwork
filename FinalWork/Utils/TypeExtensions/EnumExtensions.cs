﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace FinalWork.Utils.TypeExtensions
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum enumValue, bool upperCase = false)
        {
            var result = string.Empty;

            if (enumValue == null)
                return result;

            var member = enumValue.GetType().GetMember(enumValue.ToString()).FirstOrDefault();
            if (member == null)
            {
                result = enumValue.ToString();
            }
            else
            {
                var displayAttribute = member.GetCustomAttribute<DisplayAttribute>();
                var name = displayAttribute != null ? displayAttribute.Name : string.Empty;

                if (displayAttribute == null || string.IsNullOrEmpty(name))
                {
                    var descriptionAttribute = member.GetCustomAttribute<DescriptionAttribute>();
                    name = descriptionAttribute != null ? descriptionAttribute.Description : string.Empty;
                }

                result = !string.IsNullOrEmpty(name) ? name : enumValue.ToString();
            }

            return upperCase ? result.ToUpper() : result;
        }

        public static List<TEnum> AsList<TEnum>()
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("TEnum должен быть перечислением (System.Enum)!");

            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>().ToList();
        }
    }
}
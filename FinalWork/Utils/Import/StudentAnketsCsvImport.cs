﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using FinalWork.Model;
using FinalWork.Utils.Import.Interfaces;

namespace FinalWork.Utils.Import
{
    public class StudentAnketsCsvImport : CsvImport<Student>,
         IImporter<Student>

    {
        public StudentAnketsCsvImport()
        {
            TypedProcessor.Add(typeof(Gender), SetGender);
        }
        
        private void SetGender(object model, PropertyInfo propertyInfo, string colValue)
        {
            var attr = propertyInfo.GetCustomAttributes(typeof(ForeignKeyAttribute)).FirstOrDefault();
            if (attr == null) return;

            var foreignKeyAttribute = (ForeignKeyAttribute)attr;
            var prop = model.GetType().GetProperties().FirstOrDefault(p => p.CanWrite &&
                                                                           string.Equals(p.Name, foreignKeyAttribute.Name,
                                                                               StringComparison.InvariantCultureIgnoreCase));
            if (prop == null) return;

            var id = colValue.Substring(0, 1).Equals("м", StringComparison.InvariantCultureIgnoreCase)
                ? Constants.Ids.GenderId.Male
                : Constants.Ids.GenderId.Female;

            propertyInfo.SetValue(model, id);
        }
    }
}

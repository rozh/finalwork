using System.Collections.Generic;
using FinalWork.Model.Base;

namespace FinalWork.Utils.Import.Interfaces
{
    public interface IImporter<TModel> where TModel : ModelBase
    {
        IEnumerable<TModel> Import(string fileName, Dictionary<string, object> defaultPropertyValuesDictionary);
        IEnumerable<TModel> Merge(IEnumerable<TModel> left, IEnumerable<TModel> right, string[] keys, string[] excludeProps);
    }
}
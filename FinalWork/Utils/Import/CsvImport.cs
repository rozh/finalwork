﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using FinalWork.Model.Base;
using FinalWork.Utils.Import.Interfaces;

namespace FinalWork.Utils.Import
{
    public class CsvImport<TModel> 
        : IImporter<TModel> 
        where TModel : ModelBase
    {
        protected readonly Dictionary<Type, Action<object, PropertyInfo, string>> TypedProcessor;
        public char Separator { get; set; } = ',';

        public CsvImport()
        {
            TypedProcessor = new Dictionary<Type, Action<object, PropertyInfo, string>>();
            TypedProcessor.Add(typeof(DateTime), (model, propertyInfo, colValue) => { propertyInfo.SetValue(model, DateTime.Parse(colValue)); });
            TypedProcessor.Add(typeof(DateTime?), (model, propertyInfo, colValue) => { propertyInfo.SetValue(model, DateTime.Parse(colValue)); });
        }

        public IEnumerable<TModel> Import(string fileName, Dictionary<string, object> defaultPropertyValuesDictionary)
        {
            var result = new List<TModel>();
            var lines = File.ReadAllLines(fileName);
            var names = lines[0].Split(Separator);
            for (var i = 1; i < lines.Length; i++)
            {
                var line = lines[i];
                var cols = line.Split(Separator);
                var model = (TModel)Activator.CreateInstance(typeof(TModel));
                SetDefaultProperties(model, defaultPropertyValuesDictionary);
                SetProperties(model, names, cols);
                result.Add(model);
            }
            return result;
        }

        private void SetProperties(object model, string[] names, string[] cols)
        {
            var properties = model.GetType().GetProperties();
            for (int i = 0; i < cols.Length; i++)
            {
                var propertyInfo = properties.FirstOrDefault(p => p.CanWrite &&
                                                                    string.Equals(p.Name, names[i],
                                                                        StringComparison.InvariantCultureIgnoreCase));
                if (propertyInfo != null)
                {
                    var type = propertyInfo.PropertyType;
                    if (TypedProcessor.ContainsKey(type))
                    {
                        TypedProcessor[type].Invoke(model, propertyInfo, cols[i]);
                    }
                    else if (type == typeof(string))
                    {
                        propertyInfo.SetValue(model, cols[i]);
                    }
                }
            }
        }

        private void SetDefaultProperties(object model, Dictionary<string, object> defaultPropertyValuesDictionary)
        {
            var properties = model.GetType().GetProperties();
            foreach (var defaultProperty in defaultPropertyValuesDictionary)
            {
                var propertyInfo = properties.FirstOrDefault(p => p.CanWrite &&
                                                                  string.Equals(p.Name, defaultProperty.Key,
                                                                      StringComparison.InvariantCultureIgnoreCase));
                if (propertyInfo != null)
                {
                    propertyInfo.SetValue(model, defaultProperty.Value);
                }
            }
        }

        public IEnumerable<TModel> Merge(IEnumerable<TModel> left, IEnumerable<TModel> right, string[] keys, string[] excludeProps)
        {
            var result = new List<TModel>();

            var properties = typeof(TModel).GetProperties().Where(p=>p.CanWrite && !excludeProps.Contains(p.Name, new KeyComparer())).ToList();
            var keyProperties = typeof(TModel).GetProperties().Where(p => keys.Contains(p.Name, new KeyComparer())).ToList();

            var leftModels = left.ToList();
            foreach (var rightModel in right)
            {
                var foundInLeftModel = false;
                foreach (var leftModel in leftModels)
                {
                    var equalByKey = true;
                    foreach (var keyProp in keyProperties)
                    {
                        var type = keyProp.PropertyType;
                        if (type == typeof(string))
                        {
                            if (!string.Equals((string) keyProp.GetValue(leftModel),
                                (string) keyProp.GetValue(rightModel), StringComparison.InvariantCultureIgnoreCase))
                            {
                                equalByKey = false;
                            }
                        }
                        else if (keyProp.GetValue(leftModel) != keyProp.GetValue(rightModel))
                        {
                            equalByKey = false;
                        }

                        if (!equalByKey) break;
                    }
                    if (equalByKey)
                    {
                        foundInLeftModel = true;
                        GetValue(properties, leftModel, rightModel);
                        result.Add(leftModel);
                    }
                    else
                    {
                        result.Add(leftModel);
                    }
                }
                if (!foundInLeftModel)
                {
                    var model = (TModel)Activator.CreateInstance(typeof(TModel));
                    GetValue(properties, model, rightModel);
                    result.Add(model);
                }
            }


            return result;
        }

        private static void GetValue(List<PropertyInfo> properties, TModel leftModel, TModel rightModel)
        {
            foreach (var propertyInfo in properties)
            {
                var val = propertyInfo.GetValue(rightModel);
                if (val != null)
                {
                    var str = val as string;
                    if (str != null)
                    {
                        if (!string.IsNullOrWhiteSpace(str))
                        {
                            propertyInfo.SetValue(leftModel, val);
                        }
                    }
                    else
                    {
                        propertyInfo.SetValue(leftModel, val);
                    }
                }
            }
        }

        class KeyComparer : IEqualityComparer<string>
        {
            /// <summary>Определяет, равны ли два указанных объекта.</summary>
            /// <returns>true, если указанные объекты равны; в противном случае — false.</returns>
            /// <param name="x">Первый сравниваемый объект типа <paramref name="x" />.</param>
            /// <param name="y">Второй сравниваемый объект типа <paramref name="y" />.</param>
            public bool Equals(string x, string y)
            {
                return string.Equals(x, y, StringComparison.InvariantCultureIgnoreCase);
            }

            /// <summary>Возвращает хэш-код указанного объекта.</summary>
            /// <returns>Хэш-код указанного объекта.</returns>
            /// <param name="obj">Объект <see cref="T:System.Object" />, для которого необходимо возвратить хэш-код.</param>
            /// <exception cref="T:System.ArgumentNullException">Тип параметра <paramref name="obj" /> является ссылочным типом и значение параметра <paramref name="obj" /> — null.</exception>
            public int GetHashCode(string obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}

﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using FinalWork.Utils.WinApi._Enums;
using FinalWork.Utils.WinApi._Interfaces;
using FinalWork.Utils.WinApi._Structures;

namespace FinalWork.Utils.WinApi
{
    public sealed class Taskbar : ITaskbar
    {
        private const string ClassName = "Shell_TrayWnd";

        public Taskbar()
        {
            var taskbarHandle = User32.FindWindow(ClassName, null);

            var data = new APPBARDATA();
            data.cbSize = (uint) Marshal.SizeOf(typeof (APPBARDATA));
            data.hWnd = taskbarHandle;
            var result = Shell32.SHAppBarMessage(ABM.GetTaskbarPos, ref data);
            if (result == IntPtr.Zero)
                throw new InvalidOperationException();

            Position = (TaskbarPosition) data.uEdge;
            Bounds = Rectangle.FromLTRB(data.rc.left, data.rc.top, data.rc.right, data.rc.bottom);

            data.cbSize = (uint) Marshal.SizeOf(typeof (APPBARDATA));
            result = Shell32.SHAppBarMessage(ABM.GetState, ref data);
            var state = result.ToInt32();
            AlwaysOnTop = (state & ABS.AlwaysOnTop) == ABS.AlwaysOnTop;
            AutoHide = (state & ABS.Autohide) == ABS.Autohide;
        }

        public Rectangle Bounds { get; }

        public TaskbarPosition Position { get; }

        public Point Location
        {
            get { return Bounds.Location; }
        }

        /// <summary>
        ///     Размер панели задач
        /// </summary>
        public Size Size
        {
            get { return Bounds.Size; }
        }

        //Всегда возвращает false под Windows 7 (проверялось на Windows 7 RC Ultimate)
        public bool AlwaysOnTop { get; }

        /// <summary>
        ///     Скрывается ли панель задач автоматически
        /// </summary>
        public bool AutoHide { get; }
    }

    public static class ABS
    {
        public const int Autohide = 0x0000001;
        public const int AlwaysOnTop = 0x0000002;
    }
}
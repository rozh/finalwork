﻿using System;
using System.Runtime.InteropServices;
using FinalWork.Utils.WinApi._Enums;
using FinalWork.Utils.WinApi._Structures;

namespace FinalWork.Utils.WinApi
{
    public static class Shell32
    {
        [DllImport("shell32.dll", SetLastError = true)]
        public static extern IntPtr SHAppBarMessage(ABM dwMessage, [In] ref APPBARDATA pData);
    }
}
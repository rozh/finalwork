﻿namespace FinalWork.Utils.WinApi._Enums
{
    public enum TaskbarPosition
    {
        Unknown = -1,
        Left,
        Top,
        Right,
        Bottom
    }
}
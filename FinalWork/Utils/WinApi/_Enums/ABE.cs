﻿namespace FinalWork.Utils.WinApi._Enums
{
    public enum ABE : uint
    {
        Left = 0,
        Top = 1,
        Right = 2,
        Bottom = 3
    }
}
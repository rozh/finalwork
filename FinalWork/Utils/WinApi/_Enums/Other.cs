﻿namespace FinalWork.Utils.WinApi._Enums
{
    static public class Other
    {
        public const uint HWND_BROADCAST = 0xFFFF;
        public const short SW_RESTORE = 9;
    }
}
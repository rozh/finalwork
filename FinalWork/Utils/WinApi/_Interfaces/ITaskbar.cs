﻿using System.Drawing;
using FinalWork.Utils.WinApi._Enums;

namespace FinalWork.Utils.WinApi._Interfaces
{
    public interface ITaskbar
    {
        Rectangle Bounds { get; }

        TaskbarPosition Position { get; }

        Point Location { get; }

        /// <summary>
        ///     Размер панели задач
        /// </summary>
        Size Size { get; }

        bool AlwaysOnTop { get; }

        /// <summary>
        ///     Скрывается ли панель задач автоматически
        /// </summary>
        bool AutoHide { get; }
    }
}
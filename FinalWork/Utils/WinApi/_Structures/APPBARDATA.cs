﻿using System;
using System.Runtime.InteropServices;
using FinalWork.Utils.WinApi._Enums;

namespace FinalWork.Utils.WinApi._Structures
{
    [StructLayout(LayoutKind.Sequential)]
    public struct APPBARDATA
    {
        public uint cbSize;
        public IntPtr hWnd;
        public uint uCallbackMessage;
        public ABE uEdge;
        public RECT rc;
        public int lParam;
    }
}
﻿using System;

namespace FinalWork.Utils
{
    public class NumericDeclensionHelper
    {
        public static string Decline(int number, string singular, string plural, string negation)
        {
            var num = Math.Abs(number);
            if (num > 10 && num < 20)
            {
                return negation;
            }
            else
            {
                var unit = num % 10;
                switch (unit)
                {
                    case 1: return singular;
                    case 2:
                    case 3:
                    case 4: return plural;
                    case 0:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10: return negation;
                }
            }
            return singular;
        }
    }
}

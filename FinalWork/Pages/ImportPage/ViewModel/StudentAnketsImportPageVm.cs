﻿using FinalWork.Database.Repository;
using FinalWork.Model;
using FinalWork.Model.Base;
using FinalWork.Utils.Import;
using FinalWork.ViewModel;

namespace FinalWork.Pages.ImportPage.ViewModel
{
    public class StudentAnketsImportPageVm
        : ImportPageVm<Student, StudentRepository, StudentVm, StudentAnketsCsvImport>
    {
        public StudentAnketsImportPageVm()
        {
            DefaultPropertyValuesDictionary.Add(nameof(Student.DepartamentId), "1927437c-bce4-43c1-b7fd-520f36380001");
            DefaultPropertyValuesDictionary.Add(nameof(Student.InstituteId), "0927437c-bce4-43c1-b7fd-520f36380001");

            MergeKeyProperties.AddRange(
                new[]
                {
                    nameof(Student.FirstName),
                    nameof(Student.LastName),
                    nameof(Student.MiddleName),
                    nameof(Student.Email)
                }
            );

            MergeExcludeProperties.AddRange(new[]
            {
                nameof(ModelBase.Id),
                nameof(CatalogBase.Deleted),
                nameof(CatalogBase.CreateDate),
                nameof(CatalogBase.UpdateDate)
            });
        }
    }
}
﻿using FinalWork.Database.Repository;
using FinalWork.Model;
using FinalWork.Utils.Import;
using FinalWork.ViewModel;

namespace FinalWork.Pages.ImportPage.ViewModel
{
    public class DesignImportPageVm : ImportPageVm<User, UserRepository, UserVm, CsvImport<User>>
    {
        public DesignImportPageVm()
        {
            
        }
    }
}

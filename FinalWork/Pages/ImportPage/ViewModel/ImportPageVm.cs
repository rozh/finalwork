﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using FinalWork.Database;
using FinalWork.Database.Repository.Base;
using FinalWork.Infrastructure;
using FinalWork.Model.Base;
using FinalWork.UI;
using FinalWork.UI.Controls.AutoGenDataGrid;
using FinalWork.UI.Utils;
using FinalWork.UI.Workspaces;
using FinalWork.Utils.Attributes;
using FinalWork.Utils.Import.Interfaces;
using FinalWork.ViewModel.Base;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;

namespace FinalWork.Pages.ImportPage.ViewModel
{
    public class ImportPageVm<TModel, TRepository, TViewModel, TImporter>
        : LoginSupportedWorkspace, IDisposable, INavigationPage, IAutoGenDataGridControlDataContext
        where TModel : CatalogBase
        where TRepository : IGenericRepository<TModel>
        where TViewModel : CatalogBaseVm<TModel>
        where TImporter : IImporter<TModel>
    {
        private readonly ILogger _logger;
        private Dispatcher _threadDispatcher;
        private ObservableCollection<TViewModel> _data;
        private RelayCommand _saveCommand;
        private TViewModel _selectedItem;
        private RelayCommand _reloadCommand;
        private string _filePath;
        private RelayCommand _browseCommand;

        /// <summary>
        /// Значения по умолчанию для определенного списка свойств
        /// </summary>
        protected readonly Dictionary<string, object> DefaultPropertyValuesDictionary;

        /// <summary>
        /// Ключ для поиска соответствий с имеющимися записями перед объединением
        /// </summary>
        protected readonly List<string> MergeKeyProperties;

        /// <summary>
        /// Список исключенных из копирования полей (поля не будут изменены в исходных записях)
        /// </summary>
        protected readonly List<string> MergeExcludeProperties;

        public ImportPageVm()
        {
            _threadDispatcher = Dispatcher.CurrentDispatcher;
            _data = new ObservableCollection<TViewModel>();
            ComboBoxData = new Dictionary<string, List<object>>();
            DefaultPropertyValuesDictionary = new Dictionary<string, object>();
            MergeKeyProperties = new List<string>();
            MergeExcludeProperties = new List<string>();

            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
                _data.Add(CreateDataItem());
            }
            else
            {
                // Code runs "for real"
                _logger = ServiceLocator.Instance.LogService.GetLogger(GetType());
                WindowManager.Instance.Initialize(Application.Current.MainWindow, this);
            }
        }

        public Type GetViewModelType()
        {
            return typeof(TViewModel);
        }

        public Dictionary<string, List<object>> ComboBoxData { get; }

        public ObservableCollection<TViewModel> Data
        {
            get { return _data; }
            set
            {
                _data = value;
                RaisePropertyChanged();
            }
        }
        public string FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                RaisePropertyChanged();
            }
        }

        public TViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            ClearData();
        }

        private void ClearData()
        {
            foreach (var item in Data)
            {
                item.StopListenPropertyChanging();
                item.Dispose();
            }
            Data.Clear();
            ComboBoxData.Clear();
        }

        private void LoadData()
        {
            List<TModel> items;

            LoadComboBoxData();

            var attr = (IncludePropsToLoadAttribute) typeof(TViewModel)
                .GetCustomAttributes(typeof(IncludePropsToLoadAttribute), false)
                .FirstOrDefault();
            using (var dh = new DatabaseHelper())
            {
                var repo = dh.GetRepo<TRepository>();
                items = repo.Get(includeProperties: attr == null ? string.Empty : string.Join(",", attr.Include))
                    .ToList();
            }
            
            var importer = (TImporter)Activator.CreateInstance(typeof(TImporter));
            var newItems = importer.Import(FilePath, DefaultPropertyValuesDictionary);
            var mergedItems = importer.Merge(items, newItems, MergeKeyProperties.ToArray(),
                MergeExcludeProperties.ToArray());

            foreach (var item in mergedItems)
            {
                var vmItem = CreateDataItem(item);
                Data.Add(vmItem);
                vmItem.StartListenPropertyChanging();
            }
        }

        private void LoadComboBoxData()
        {
            var propertyInfos = typeof(TViewModel).GetProperties()
                .Where(p => p.GetCustomAttributes(typeof(ComboBoxAttribute), false).Length > 0);
            foreach (var propertyInfo in propertyInfos)
            {
                var comboboxAttr = (ComboBoxAttribute) propertyInfo
                    .GetCustomAttributes(typeof(ComboBoxAttribute), false)
                    .FirstOrDefault();
                if (comboboxAttr != null)
                {
                    List<object> data = null;
                    using (var dh = new DatabaseHelper())
                    {
                        dynamic repo = dh.GetRepo(comboboxAttr.ModelType);
                        var objects = repo.Get() as IEnumerable<ModelBase>;
                        if (objects != null)
                            data = objects.Select(o => CreateDataObject(o, propertyInfo.PropertyType)).ToList();
                    }
                    if (comboboxAttr.ModelType.FullName != null && !ComboBoxData.ContainsKey(comboboxAttr.ModelType.FullName))
                    {
                        if (data != null) ComboBoxData.Add(comboboxAttr.ModelType.FullName, new List<object>(data));
                    }
                }
            }
        }

        private TViewModel CreateDataItem()
        {
            var model = (TModel) Activator.CreateInstance(typeof(TModel));
            var item = CreateDataItem(model);
            item.IsNew = true;
            return item;
        }

        private TViewModel CreateDataItem(TModel model)
        {
            return (TViewModel) Activator.CreateInstance(typeof(TViewModel), model);
        }

        private object CreateDataObject(ModelBase model, Type vmType)
        {
            return Activator.CreateInstance(vmType, model);
        }
        
        public RelayCommand SaveCommand
        {
            get
            {
                return _saveCommand ??
                       (_saveCommand = new RelayCommand(() =>
                       {
                           using (var dh = new DatabaseHelper())
                           {
                               var repo = dh.GetRepo<TRepository>();
                               foreach (var item in Data)
                               {
                                   repo.AddOrUpdate(item.Model);
                               }
                               dh.SaveChanges();
                           }
                       }));
            }
        }

        public RelayCommand ReloadCommand
        {
            get
            {
                return _reloadCommand ??
                       (_reloadCommand = new RelayCommand(() =>
                       {
                           ClearData();
                           LoadData();
                       }));
            }
        }

        public RelayCommand BrowseCommand
        {
            get
            {
                return _browseCommand ??
                       (_browseCommand = new RelayCommand(() =>
                       {
                           var fileDialog = new OpenFileDialog()
                           {
                               AddExtension = true,
                               CheckFileExists = false,
                               CheckPathExists = true,
                               DefaultExt = ".csv",
                               Title = "Импорт анкетных данных",
                               ValidateNames = true,
                               Filter = "CSV Data|*.csv",
                           };
                           if (fileDialog.ShowDialog() == true)
                           {
                               FilePath = fileDialog.FileName;
                               LoadData();
                           }
                       }));
            }
        }

        public void SetParams(object parameter)
        {
            string title = parameter as string;
            if (title != null)
            {
                Title = title;
            }
        }

        public void Navigated()
        {
            ClearData();
            LoadComboBoxData();
        }
    }
}
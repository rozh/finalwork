﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using FinalWork.Database;
using FinalWork.Database.Repository.Base;
using FinalWork.Infrastructure;
using FinalWork.Model.Base;
using FinalWork.UI;
using FinalWork.UI.Controls.AutoGenDataGrid;
using FinalWork.UI.Utils;
using FinalWork.UI.Workspaces;
using FinalWork.Utils.Attributes;
using FinalWork.Utils.TypeExtensions;
using FinalWork.ViewModel.Base;
using GalaSoft.MvvmLight.Command;

namespace FinalWork.Pages.CatalogPage
{
    public class CatalogPageVm<TModel, TRepository, TViewModel>
        : LoginSupportedWorkspace, IDisposable, INavigationPage, IAutoGenDataGridControlDataContext
        where TModel : CatalogBase
        where TRepository : IGenericRepository<TModel>
        where TViewModel : CatalogBaseVm<TModel>
    {
        private readonly ILogger _logger;
        private Dispatcher _threadDispatcher;
        private ObservableCollection<TViewModel> _data;
        private readonly List<TViewModel> _deleteList;
        private RelayCommand _addCommand;
        private RelayCommand _saveCommand;
        private RelayCommand<TViewModel> _deleteCommand;
        private TViewModel _selectedItem;
        private RelayCommand _reloadCommand;
        private RelayCommand _copyCommand;

        public CatalogPageVm()
        {
            _threadDispatcher = Dispatcher.CurrentDispatcher;
            _deleteList = new List<TViewModel>();
            _data = new ObservableCollection<TViewModel>();
            ComboBoxData = new Dictionary<string, List<object>>();

            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
                _data.Add(CreateDataItem());
            }
            else
            {
                // Code runs "for real"
                _logger = ServiceLocator.Instance.LogService.GetLogger(GetType());
                WindowManager.Instance.Initialize(Application.Current.MainWindow, this);
            }
        }

        public Type GetViewModelType()
        {
            return typeof(TViewModel);
        }

        public Dictionary<string, List<object>> ComboBoxData { get; }

        public ObservableCollection<TViewModel> Data
        {
            get { return _data; }
            set
            {
                _data = value;
                RaisePropertyChanged();
            }
        }

        public TViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            ClearData();
        }

        private void ClearData()
        {
            foreach (var item in Data)
            {
                item.StopListenPropertyChanging();
                item.Dispose();
            }
            Data.Clear();
            ComboBoxData.Clear();
            _deleteList.Clear();
        }

        private void LoadData()
        {
            List<TViewModel> items;

            var propertyInfos = typeof(TViewModel).GetProperties()
                .Where(p => p.GetCustomAttributes(typeof(ComboBoxAttribute), false).Length > 0);
            foreach (var propertyInfo in propertyInfos)
            {
                var comboboxAttr = (ComboBoxAttribute) propertyInfo
                    .GetCustomAttributes(typeof(ComboBoxAttribute), false)
                    .FirstOrDefault();
                if (comboboxAttr != null)
                {
                    List<object> data = null;
                    using (var dh = new DatabaseHelper())
                    {
                        dynamic repo = dh.GetRepo(comboboxAttr.ModelType);
                        var objects = repo.Get() as IEnumerable<ModelBase>;
                        if (objects != null)
                            data = objects.Select(o => CreateDataObject(o, propertyInfo.PropertyType)).ToList();
                    }
                    if (comboboxAttr.ModelType.FullName != null && !ComboBoxData.ContainsKey(comboboxAttr.ModelType.FullName))
                    {
                        if (data != null) ComboBoxData.Add(comboboxAttr.ModelType.FullName, new List<object>(data));
                    }
                }
            }

            var attr = (IncludePropsToLoadAttribute) typeof(TViewModel)
                .GetCustomAttributes(typeof(IncludePropsToLoadAttribute), false)
                .FirstOrDefault();
            using (var dh = new DatabaseHelper())
            {
                var repo = dh.GetRepo<TRepository>();
                items = repo.Get(includeProperties: attr == null ? string.Empty : string.Join(",", attr.Include))
                    .Select(CreateDataItem)
                    .ToList();
            }
            foreach (var item in items)
            {
                Data.Add(item);
                item.StartListenPropertyChanging();
            }
        }

        private TViewModel CreateDataItem()
        {
            var model = (TModel) Activator.CreateInstance(typeof(TModel));
            var item = CreateDataItem(model);
            item.IsNew = true;
            return item;
        }

        private TViewModel CreateDataItem(TModel model)
        {
            return (TViewModel) Activator.CreateInstance(typeof(TViewModel), model);
        }

        private object CreateDataObject(ModelBase model, Type vmType)
        {
            return Activator.CreateInstance(vmType, model);
        }

        public RelayCommand AddCommand
        {
            get
            {
                return _addCommand ??
                       (_addCommand = new RelayCommand(() =>
                       {
                           Data.Add(CreateDataItem());
                       }));
            }
        }

        public RelayCommand CopyCommand
        {
            get
            {
                return _copyCommand ??
                       (_copyCommand = new RelayCommand(() =>
                       {
                           var model = (TModel)Activator.CreateInstance(typeof(TModel));
                           SelectedItem.Model.CopyPropertiesTo(model);
                           var newItem = CreateDataItem(model);
                           newItem.Model.Id = Guid.NewGuid().ToString("D");
                           newItem.IsNew = true;
                           Data.Add(newItem);
                       }));
            }
        }

        public RelayCommand<TViewModel> DeleteCommand
        {
            get
            {
                return _deleteCommand ??
                       (_deleteCommand = new RelayCommand<TViewModel>(arg =>
                       {
                           if (arg == null) return;
                           _deleteList.Add(arg);

                           var index = Data.IndexOf(arg);
                           Data.Remove(arg);
                           if (index < Data.Count)
                           {
                               SelectedItem = Data[index];
                           }
                           else
                           {
                               SelectedItem = Data.Count > 0 ? Data[Data.Count - 1] : null;
                           }
                       }));
            }
        }

        public RelayCommand SaveCommand
        {
            get
            {
                return _saveCommand ??
                       (_saveCommand = new RelayCommand(() =>
                       {
                           var newItems = Data.Where(i => i.IsNew).ToList();
                           var updatedItems = Data.Where(i => i.IsEdited && !i.IsNew).ToList();
                           using (var dh = new DatabaseHelper())
                           {
                               var repo = dh.GetRepo<TRepository>();
                               foreach (var item in updatedItems)
                               {
                                   repo.Update(item.Model);
                               }
                               foreach (var item in newItems)
                               {
                                   repo.Add(item.Model);
                               }
                               foreach (var item in _deleteList)
                               {
                                   repo.Delete(item.Model);
                               }
                               dh.SaveChanges();
                           }

                           foreach (var item in updatedItems)
                           {
                               item.IsEdited = false;
                               item.IsNew = false;
                           }
                           foreach (var item in newItems)
                           {
                               item.IsEdited = false;
                               item.IsNew = false;
                           }
                           _deleteList.Clear();
                       }));
            }
        }

        public RelayCommand ReloadCommand
        {
            get
            {
                return _reloadCommand ??
                       (_reloadCommand = new RelayCommand(() =>
                       {
                           ClearData();
                           LoadData();
                       }));
            }
        }

        public void SetParams(object parameter)
        {
            //ignored
        }

        public void Navigated()
        {
            ClearData();
            LoadData();
        }
    }
}
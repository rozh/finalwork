﻿using System;
using FinalWork.Model.Base;

namespace FinalWork.ViewModel.Base
{
    public class CatalogBaseVm<TModel>
        : EditableItemBaseVm
        where TModel : CatalogBase
    {
        public CatalogBaseVm(TModel model)
        {
            Model = model;
        }
        
        public string Id => Model.Id;

        public TModel Model { get; }
    }
}
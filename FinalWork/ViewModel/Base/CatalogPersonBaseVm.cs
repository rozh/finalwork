﻿using System.ComponentModel;
using FinalWork.Model.Base;
using FinalWork.Utils.Attributes;

namespace FinalWork.ViewModel.Base
{
    public abstract class CatalogPersonBase<T>
        : CatalogBaseVm<T>
        where T : CatalogPersonBase
    {
        /// <summary>
        ///     Имя
        /// </summary>
        [ColumnAttribute(1, "Имя")]
        public string FirstName
        {
            get { return Model.FirstName; }
            set
            {
                Model.FirstName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Отчество
        /// </summary>
        [ColumnAttribute(2, "Отчество")]
        public string MiddleName
        {
            get { return Model.MiddleName; }
            set
            {
                Model.MiddleName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Фамилия
        /// </summary>
        [ColumnAttribute(3, "Фамилия")]
        public string LastName
        {
            get { return Model.LastName; }
            set
            {
                Model.LastName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Фамилия и инициалы
        /// </summary>
        public string FullNameShort
            =>
                (LastName.Length > 0 ? $"{LastName} " : "") +
                (FirstName.Length > 0 ? $"{FirstName[0]}. " : "") +
                (MiddleName.Length > 0 ? $"{MiddleName[0]}." : "");

        /// <summary>
        ///     Фамилия Имя Отчество
        /// </summary>
        public string FullName => $"{LastName} {FirstName} {MiddleName}";

        protected CatalogPersonBase(T model) : base(model)
        {
        }
    }
}
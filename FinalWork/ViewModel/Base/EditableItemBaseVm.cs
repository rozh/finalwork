﻿using System.Collections.Generic;
using System.ComponentModel;
using FinalWork.Infrastructure;
using GalaSoft.MvvmLight;

namespace FinalWork.ViewModel.Base
{
    public abstract class EditableItemBaseVm : ViewModelBase
    {
        public EditableItemBaseVm()
        {
            ExcludePropertyList = new List<string>();
            Logger = ServiceLocator.Instance.LogService.GetLogger(GetType());
        }
        
        private bool _isEdited;
        private bool _listeningStarted;
        private bool _isNew;

        public event PropertyChangedEventHandler ActualPropertyChanged;

        protected void OnActualPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = ActualPropertyChanged;
            if (handler != null) handler(this, e);
        }

        protected ILogger Logger { get; }

        public List<string> ExcludePropertyList { get; protected set; }

        public bool IsNew
        {
            get { return _isNew; }
            set
            {
                _isNew = value;
                RaisePropertyChanged();
            }
        }
        
        /// <summary>Показывает, что элемент был отредактирован, и не был сохранён, например, в базу данных</summary>
        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                _isEdited = value;
                RaisePropertyChanged();
            }
        }

        public bool PropertyChangingListeningStarted
        {
            get { return _listeningStarted; }
            set { _listeningStarted = value; }
        }

        /// <summary>
        /// Начинает прослушивать изменения свойств объекта. В случае изменения свойства, которое не попадает в список исключений 
        /// <code>ExcludePropertyList</code>, свойство <code>WasEditedWithoutSaving</code> получает значение <code>true</code>.
        /// И генерируется событие <code>ActualPropertyChanged</code>.
        /// </summary>
        public void StartListenPropertyChanging()
        {
            if (!_listeningStarted)
            {
                PropertyChanged += EditableItemViewModelBase_PropertyChanged;
                _listeningStarted = true;
            }
            else
            {
                Logger.Error("Попытка подписаться на прослушивание изменений свойств несколько раз подряд предварительно не отписавшись");
            }
        }

        public void StopListenPropertyChanging()
        {
            if (_listeningStarted)
            {
                PropertyChanged -= EditableItemViewModelBase_PropertyChanged;
                _listeningStarted = false;
            }
            else
            {
                Logger.Error("Попытка отписаться на прослушивание изменений свойств несколько раз подряд предварительно не подписавшись");
            }
        }

        private void EditableItemViewModelBase_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(IsNew) && e.PropertyName != nameof(IsEdited))
            {
                bool isExcluded = false;
                for (int i = 0; i < ExcludePropertyList.Count; i++)
                {
                    if (ExcludePropertyList[i] == e.PropertyName)
                    {
                        isExcluded = true;
                        break;
                    }
                }
                if (!isExcluded)
                {
                    IsEdited = true;

                    OnActualPropertyChanged(e);
                }
            }
        }

        protected virtual void OnDispose()
        {
        }

        public void Dispose()
        {
            if (_listeningStarted)
                PropertyChanged -= EditableItemViewModelBase_PropertyChanged;

            OnDispose();
        }
    }
}
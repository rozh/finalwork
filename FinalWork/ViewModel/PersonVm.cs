﻿using System.ComponentModel.DataAnnotations;
using FinalWork.Model;
using FinalWork.Utils.Attributes;
using FinalWork.ViewModel.Base;

namespace FinalWork.ViewModel
{
    /// <summary>
    ///     Персона
    /// </summary>
    public class PersonVm : CatalogBaseVm<Person>
    {
        /// <summary>
        ///     Имя
        /// </summary>
        [Column(1, "Имя")]
        [MaxLength(100)]
        public string FirstName
        {
            get { return Model.FirstName; }
            set
            {
                Model.FirstName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Отчество
        /// </summary>
        [Column(2, "Отчество")]
        [MaxLength(100)]
        public string MiddleName
        {
            get { return Model.MiddleName; }
            set
            {
                Model.MiddleName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Фамилия
        /// </summary>
        [Column(3, "Фамилия")]
        [MaxLength(100)]
        public string LastName
        {
            get { return Model.LastName; }
            set
            {
                Model.LastName = value;
                RaisePropertyChanged();
            }
        }


        #region Dative

        /// <summary>
        ///     Имя в дательном падеже
        /// </summary>
        [Column(11, "Имя (д.п.)")]
        [MaxLength(100)]
        public string DativeFirstName
        {
            get { return Model.DativeFirstName; }
            set
            {
                Model.DativeFirstName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Отчество в дательном падеже
        /// </summary>
        [Column(12, "Отчество (д.п.)")]
        [MaxLength(100)]
        public string DativeMiddleName
        {
            get { return Model.DativeMiddleName; }
            set
            {
                Model.DativeMiddleName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Фамилия в дательном падеже
        /// </summary>
        [Column(13, "Фамилия (д.п.)")]
        [MaxLength(100)]
        public string DativeLastName
        {
            get { return Model.DativeLastName; }
            set
            {
                Model.DativeLastName = value;
                RaisePropertyChanged();
            }
        }
        
        #endregion

        #region Genitive

        /// <summary>
        ///     Имя в родительном падеже
        /// </summary>
        [Column(21, "Имя (р.п.)")]
        [MaxLength(100)]
        public string GenitiveFirstName
        {
            get { return Model.GenitiveFirstName; }
            set
            {
                Model.GenitiveFirstName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Отчество в родительном падеже
        /// </summary>
        [Column(22, "Отчество (р.п.)")]
        [MaxLength(100)]
        public string GenitiveMiddleName
        {
            get { return Model.GenitiveMiddleName; }
            set
            {
                Model.GenitiveMiddleName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Фамилия в родительном падеже
        /// </summary>
        [Column(23, "Фамилия (р.п.)")]
        [MaxLength(100)]
        public string GenitiveLastName
        {
            get { return Model.GenitiveLastName; }
            set
            {
                Model.GenitiveLastName = value;
                RaisePropertyChanged();
            }
        }
        
        #endregion

        #region Instrumental

        /// <summary>
        ///     Имя в творительном падеже
        /// </summary>
        [Column(31, "Имя (т.п.)")]
        [MaxLength(100)]
        public string InstrumentalFirstName
        {
            get { return Model.InstrumentalFirstName; }
            set
            {
                Model.InstrumentalFirstName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Отчество в творительном падеже
        /// </summary>
        [Column(32, "Отчество (т.п.)")]
        [MaxLength(100)]
        public string InstrumentalMiddleName
        {
            get { return Model.InstrumentalMiddleName; }
            set
            {
                Model.InstrumentalMiddleName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Фамилия в творительном падеже
        /// </summary>
        [Column(33, "Фамилия (т.п.)")]
        [MaxLength(100)]
        public string InstrumentalLastName
        {
            get { return Model.InstrumentalLastName; }
            set
            {
                Model.InstrumentalLastName = value;
                RaisePropertyChanged();
            }
        }

        #endregion


        /// <summary>
        ///     Ученая степень
        /// </summary>
        [Column(41, "Ученая степень")]
        [MaxLength(255)]
        public string AcademicDegree
        {
            get { return Model.AcademicDegree; }
            set
            {
                Model.AcademicDegree = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Ученое звание
        /// </summary>
        [Column(42, "Ученое звание")]
        [MaxLength(255)]
        public string AcademicRank
        {
            get { return Model.AcademicRank; }
            set
            {
                Model.AcademicRank = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Организация
        /// </summary>
        [Column(43, "Организация")]
        [MaxLength(255)]
        public string Organization
        {
            get { return Model.Organization; }
            set
            {
                Model.Organization = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Должность
        /// </summary>
        [Column(44, "Должность")]
        [MaxLength(255)]
        public string Position
        {
            get { return Model.Position; }
            set
            {
                Model.Position = value;
                RaisePropertyChanged();
            }
        }

        public PersonVm(Person model) : base(model)
        {
        }
    }
}
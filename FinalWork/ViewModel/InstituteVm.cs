﻿using FinalWork.Model;
using FinalWork.Utils.Attributes;
using FinalWork.ViewModel.Base;

namespace FinalWork.ViewModel
{
    public class InstituteVm : CatalogBaseVm<Institute>
    {
        [Column(1, "Название института")]
        public string Name
        {
            get { return Model.Name; }
            set
            {
                Model.Name = value;
                RaisePropertyChanged();
            }
        }

        [Column(2, "Короткое название")]
        public string ShortName
        {
            get { return Model.ShortName; }
            set
            {
                Model.ShortName = value;
                RaisePropertyChanged();
            }
        }

        public InstituteVm(Institute model) : base(model)
        {
        }
    }
}
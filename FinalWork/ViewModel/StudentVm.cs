﻿using System.ComponentModel.DataAnnotations;
using FinalWork.Model;
using FinalWork.Utils.Attributes;
using FinalWork.ViewModel.Base;

namespace FinalWork.ViewModel
{
    public class StudentVm
        : CatalogPersonBase<Student>
    {
        private InstituteVm _institute;
        private DepartamentVm _departament;

        public StudentVm(Student student) : base(student)
        {
            _institute = new InstituteVm(student.Institute);
            ExcludePropertyList.Add(nameof(Institute));
        }

        /// <summary>
        ///     Институт
        /// </summary>
        [ColumnAttribute(101,"Институт")]
        [ComboBoxAttribute(nameof(InstituteId), nameof(InstituteVm.Name), nameof(InstituteVm.Id), typeof(Institute))]
        public InstituteVm Institute
        {
            get { return _institute; }
            set
            {
                _institute = value;
                RaisePropertyChanged();
            }
        }

        public string InstituteId
        {
            get { return Model.InstituteId; }
            set
            {
                Model.InstituteId = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Институт
        /// </summary>
        [ColumnAttribute(102, "Кафедра")]
        [ComboBoxAttribute(nameof(DepartamentId), nameof(DepartamentVm.Name), nameof(DepartamentVm.Id), typeof(Departament))]
        public DepartamentVm Departament
        {
            get { return _departament; }
            set
            {
                _departament = value;
                RaisePropertyChanged();
            }
        }

        public string DepartamentId
        {
            get { return Model.DepartamentId; }
            set
            {
                Model.DepartamentId = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Группа
        /// </summary>
        [ColumnAttribute(103, "Группа")]
        [MaxLength(10)]
        public string Group
        {
            get { return Model.Group; }
            set
            {
                Model.Group = value;
                RaisePropertyChanged();
            }
        }


        /// <summary>
        ///     Форма обучения
        /// </summary>
        [ColumnAttribute(104, "Форма обучения")]
        [MaxLength(10)]
        public string EducationType
        {
            get { return Model.EducationType; }
            set
            {
                Model.EducationType = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Имя в дательном падеже
        /// </summary>
        [ColumnAttribute(106, "Имя (д.п.)")]
        [MaxLength(100)]
        public string DativeFirstName
        {
            get { return Model.DativeFirstName; }
            set
            {
                Model.DativeFirstName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Отчество в дательном падеже
        /// </summary>
        [ColumnAttribute(107, "Отчество (д.п.)")]
        [MaxLength(100)]
        public string DativeMiddleName
        {
            get { return Model.DativeMiddleName; }
            set
            {
                Model.DativeMiddleName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Фамилия в дательном падеже
        /// </summary>
        [ColumnAttribute(108, "Фамилия (д.п.)")]
        [MaxLength(100)]
        public string DativeLastName
        {
            get { return Model.DativeLastName; }
            set
            {
                Model.DativeLastName = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Фамилия и инициалы в дательном падеже
        /// </summary>
        public string DativeFullNameShort
            =>
                (DativeLastName.Length > 0 ? $"{DativeLastName} " : "") +
                (DativeFirstName.Length > 0 ? $"{DativeFirstName[0]}. " : "") +
                (DativeMiddleName.Length > 0 ? $"{DativeMiddleName[0]}." : "");

        /// <summary>
        ///     Фамилия Имя Отчество в дательном падеже
        /// </summary>
        public string DativeFullName => $"{DativeLastName} {DativeFirstName} {DativeMiddleName}";
        
        /// <summary>
        ///     Email
        /// </summary>
        [Column(122, "Email")]
        [MaxLength(255)]
        public string Email
        {
            get { return Model.Email; }
            set
            {
                Model.Email = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Образование
        /// </summary>
        [Column(123, "Образование")]
        [MaxLength(50)]
        public string EducationStep
        {
            get { return Model.EducationStep; }
            set
            {
                Model.EducationStep = value;
                RaisePropertyChanged();
            }
        }
    }
}
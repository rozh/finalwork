﻿using FinalWork.Model;
using FinalWork.Utils.Attributes;
using FinalWork.ViewModel.Base;

namespace FinalWork.ViewModel
{
    public class DepartamentVm : CatalogBaseVm<Departament>
    {

        [Column(1, "Название кафедры")]
        public string Name
        {
            get { return Model.Name; }
            set
            {
                Model.Name = value;
                RaisePropertyChanged();
            }
        }

        [Column(2, "Короткое название")]
        public string ShortName
        {
            get { return Model.ShortName; }
            set
            {
                Model.ShortName = value;
                RaisePropertyChanged();
            }
        }
        
        public DepartamentVm(Departament model) : base(model)
        {
        }
    }
}
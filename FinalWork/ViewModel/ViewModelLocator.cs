﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using FinalWork.Database.Repository;
using FinalWork.Model;
using FinalWork.Pages.CatalogPage;
using FinalWork.Pages.ImportPage;
using FinalWork.Pages.ImportPage.ViewModel;
using FinalWork.Windows.MainWindow;
using FinalWork.UI;
using FinalWork.UI.Utils;
using FinalWork.Utils.Import;
using FinalWork.Windows.SplashScreen;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace FinalWork.ViewModel
{
    /// <summary>
    ///     This class contains static references to all the view models in the
    ///     application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        ///     Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                // Create design time view services and models
                //SimpleIoc.Default.Register<IDataService, DesignDataService>();
            }
            else
            {
                // Create run time view services and models
                //SimpleIoc.Default.Register<IDataService, DataService>();

                if (Application.Current.MainWindow != null)
                {
                    Application.Current.MainWindow.Loaded += MainWindow_Loaded;
                    Application.Current.MainWindow.Closing += MainWindow_Closing;
                }
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<CatalogPageVm<User,UserRepository, UserVm>>();
            SimpleIoc.Default.Register<CatalogPageVm<Student, StudentRepository, StudentVm>>();
            SimpleIoc.Default.Register<CatalogPageVm<Institute, InstituteRepository, InstituteVm>>();
            SimpleIoc.Default.Register<CatalogPageVm<Departament, DepartamentRepository, DepartamentVm>>();
            SimpleIoc.Default.Register<CatalogPageVm<Person, PersonRepository, PersonVm>>();
            SimpleIoc.Default.Register<ImportPageVm<Student, StudentRepository, StudentVm, CsvImport<Student>>>();
            SimpleIoc.Default.Register<StudentAnketsImportPageVm>();

            var navigationService = CreateNavigationService();
            SimpleIoc.Default.Register(() => navigationService);
        }

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public CatalogPageVm<User, UserRepository, UserVm> UserCatalog => ServiceLocator.Current.GetInstance<CatalogPageVm<User, UserRepository, UserVm>>();
        public CatalogPageVm<Student, StudentRepository, StudentVm> StudentCatalog => ServiceLocator.Current.GetInstance<CatalogPageVm<Student, StudentRepository, StudentVm>>();
        public CatalogPageVm<Institute, InstituteRepository, InstituteVm> InstituteCatalog => ServiceLocator.Current.GetInstance<CatalogPageVm<Institute, InstituteRepository, InstituteVm>>();
        public CatalogPageVm<Departament, DepartamentRepository, DepartamentVm> DepartamentCatalog => ServiceLocator.Current.GetInstance<CatalogPageVm<Departament, DepartamentRepository, DepartamentVm>>();
        public CatalogPageVm<Person, PersonRepository, PersonVm> PersonCatalog => ServiceLocator.Current.GetInstance<CatalogPageVm<Person, PersonRepository, PersonVm>>();
        public ImportPageVm<Student, StudentRepository, StudentVm, CsvImport<Student>> StudentThemesImport => ServiceLocator.Current.GetInstance<ImportPageVm<Student, StudentRepository, StudentVm, CsvImport<Student>>>();
        public StudentAnketsImportPageVm StudentImport => ServiceLocator.Current.GetInstance<StudentAnketsImportPageVm>();
        public FrameNavigationService Navigation => ServiceLocator.Current.GetInstance<FrameNavigationService>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            SplashScreenManager.SetMessage("Запуск...");
            WindowManager.Instance.Initialize(Application.Current.MainWindow, Main);

            Main.OnWindowLoaded();
            if (Application.Current?.MainWindow != null)
                Application.Current.MainWindow.Activate();

            SplashScreenManager.Close();
        }
        
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            Main.OnWindowClosing();
        }

        private FrameNavigationService CreateNavigationService()
        {
            var navigationService = new FrameNavigationService();
            navigationService.Configure("UserCatalog", CreatePageUri("Pages/CatalogPage/CatalogPageView.xaml"), UserCatalog);
            navigationService.Configure("StudentCatalog", CreatePageUri("Pages/CatalogPage/CatalogPageView.xaml"), StudentCatalog);
            navigationService.Configure("InstituteCatalog", CreatePageUri("Pages/CatalogPage/CatalogPageView.xaml"), InstituteCatalog);
            navigationService.Configure("DepartamentCatalog", CreatePageUri("Pages/CatalogPage/CatalogPageView.xaml"), DepartamentCatalog);
            navigationService.Configure("PersonCatalog", CreatePageUri("Pages/CatalogPage/CatalogPageView.xaml"), PersonCatalog);
            navigationService.Configure("StudentImport", CreatePageUri("Pages/ImportPage/View/ImportPageView.xaml"), StudentImport);
            navigationService.Configure("StudentThemesImport", CreatePageUri("Pages/ImportPage/View/ImportPageView.xaml"), StudentThemesImport);

            return navigationService;
        }

        private Uri CreatePageUri(string pagePath)
        {
            var name = Assembly.GetExecutingAssembly().GetName().Name;
            return CreatePageUri(name, pagePath);
        }

        private Uri CreatePageUri(Type vmType, string pagePath)
        {
            var name = Assembly.GetAssembly(vmType).GetName().Name;
            return CreatePageUri(name, pagePath);
        }

        private Uri CreatePageUri(string pageAssembly, string pagePath)
        {
            var uri =
                new Uri("pack://application:,,,/" + pageAssembly + ";component/" + pagePath,
                    UriKind.RelativeOrAbsolute);
            return uri;
        }
    }
}
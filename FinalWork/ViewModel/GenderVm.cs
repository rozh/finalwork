﻿using System.ComponentModel.DataAnnotations;
using FinalWork.Model;
using FinalWork.Utils.Attributes;
using FinalWork.ViewModel.Base;

namespace FinalWork.ViewModel
{
    /// <summary>
    ///     Пол
    /// </summary>
    public class GenderVm : CatalogBaseVm<Gender>
    {
        /// <summary>
        ///     Название
        /// </summary>
        [Column(1, "Название")]
        [MaxLength(255)]
        public string Name
        {
            get { return Model.Name; }
            set
            {
                Model.Name = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Короткое название 
        /// </summary>
        [Column(2, "Короткое название")]
        [MaxLength(50)]
        public string ShortName
        {
            get { return Model.ShortName; }
            set
            {
                Model.ShortName = value;
                RaisePropertyChanged();
            }
        }

        public GenderVm(Gender model) : base(model)
        {
        }
    }
}
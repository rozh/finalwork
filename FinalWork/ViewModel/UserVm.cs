﻿using System.ComponentModel;
using FinalWork.Model;
using FinalWork.Utils.Attributes;
using FinalWork.ViewModel.Base;

namespace FinalWork.ViewModel
{
    public class UserVm : CatalogBaseVm<User>
    {
        public UserVm(User model) : base(model)
        {
            
        }

        [Column(1, "Фамилия")]
        public string LastName
        {
            get { return Model.LastName; }
            set
            {
                Model.LastName = value;
                RaisePropertyChanged();
            }
        }

        [Column(2, "Имя")]
        public string FirstName
        {
            get { return Model.FirstName; }
            set
            {
                Model.FirstName = value;
                RaisePropertyChanged();
            }
        }

        [Column(3, "Отчество")]
        public string MiddleName
        {
            get { return Model.MiddleName; }
            set
            {
                Model.MiddleName = value;
                RaisePropertyChanged();
            }
        }

        [Column(4, "Логин")]
        public string Login
        {
            get { return Model.Login; }
            set
            {
                Model.Login = value;
                RaisePropertyChanged();
            }
        }


        [Column(5, "Пароль")]
        public string Password
        {
            get { return "******"; }
            set
            {
                Model.Password = User.CalculateMd5Hash(value);
                RaisePropertyChanged();
            }
        }
    }
}
﻿using System;
using System.Threading;
using System.Windows.Threading;

namespace FinalWork.Windows.SplashScreen
{
    public static class SplashScreenManager
    {
        private static SplashScreen _splashScreen;
        
        public static void Show(string initialMessage = null)
        {
            Thread thread = new Thread(
                delegate ()
                {
                    _splashScreen = new SplashScreen();

                    if (!string.IsNullOrEmpty(initialMessage))
                        _splashScreen.Message = initialMessage;

                    _splashScreen.Show();

                    Dispatcher.Run();
                });
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();
        }

        public static void Close()
        {
            if (_splashScreen == null) return;

            if (!_splashScreen.Dispatcher.CheckAccess())
            {
                Thread thread = new Thread(
                    delegate()
                    {
                        _splashScreen.Dispatcher.Invoke(
                            DispatcherPriority.Normal,
                            new Action(delegate()
                            {
                                Dispatcher.CurrentDispatcher.InvokeShutdown();
                                _splashScreen.Close();
                            }));
                    });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
            else
            {
                _splashScreen.Close();
            }
        }

        public static void SetMessage(string message)
        {
            if (_splashScreen == null) return;

            if (!_splashScreen.Dispatcher.CheckAccess())
            {
                Thread thread = new Thread(
                    delegate()
                    {
                        _splashScreen.Dispatcher.Invoke(
                            DispatcherPriority.Normal,

                            new Action(delegate()
                                {
                                    _splashScreen.Message = message;
                                }
                            ));
                        _splashScreen.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle, new Action(() => { }));
                    });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
            else
            {
                _splashScreen.Message = message;
            }
        }
    }
}
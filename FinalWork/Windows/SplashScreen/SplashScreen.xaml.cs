﻿using System;
using System.Windows;

namespace FinalWork.Windows.SplashScreen
{
    /// <summary>
    ///     Логика взаимодействия для SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof (string), typeof (SplashScreen),
                new UIPropertyMetadata(null, OnMessageChanged));

        public SplashScreen()
        {
            InitializeComponent();
            Message = "Загрузка...";
        }

        public string Message
        {
            get { return (string) GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        private static void OnMessageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var splashScreen = (SplashScreen) d;
            splashScreen.RaiseMessageChanged(EventArgs.Empty);
        }

        public event EventHandler MessageChanged;

        private void RaiseMessageChanged(EventArgs e)
        {
            var handler = MessageChanged;
            handler?.Invoke(this, e);
        }
    }
}
﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;
using FinalWork.ViewModel;

namespace FinalWork.Windows.MainWindow
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
        }
        

        protected override void OnClosed(EventArgs e)
        {
            ViewModelLocator.Cleanup();
        }

        private void MainFrame_OnLoadCompleted(object sender, NavigationEventArgs e)
        {
            UpdateFrameDataContext(sender, e);
        }

        private void MainFrame_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UpdateFrameDataContext(sender, null);
        }

        private void UpdateFrameDataContext(object sender, NavigationEventArgs e)
        {
            if (MainFrame.DataContext == null) return;
            var content = MainFrame.Content as FrameworkElement;
            if (content == null)
                return;
            content.DataContext = MainFrame.DataContext;
        }
    }
}
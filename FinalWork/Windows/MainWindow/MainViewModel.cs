﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using FinalWork.Database;
using FinalWork.Database.Repository;
using FinalWork.DocumentGenerator.Document;
using FinalWork.Infrastructure;
using FinalWork.Model;
using FinalWork.UI.Utils;
using FinalWork.UI.Workspaces;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using Microsoft.Win32;

namespace FinalWork.Windows.MainWindow
{
    /// <summary>
    ///     This class contains properties that the main View can data bind to.
    ///     <para>
    ///         Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    ///     </para>
    ///     <para>
    ///         You can also use Blend to data bind with the tool's support.
    ///     </para>
    ///     <para>
    ///         See http://www.galasoft.ch/mvvm
    ///     </para>
    /// </summary>
    public class MainViewModel : LoginSupportedWorkspace
    {
        private readonly ILogger _logger;
        private FrameNavigationService _navigationService;
        private RelayCommand<string> _pageSelectCommand;
        private RelayCommand _exportAnketCommand;

        /// <summary>
        ///     Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(FrameNavigationService navigationService)
        {
            _navigationService = navigationService;
            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
            }
            else
            {
                // Code runs "for real"
                _logger = ServiceLocator.Instance.LogService.GetLogger(GetType());

            }
        }
        
        public void OnWindowLoaded()
        {
            if (IsInDesignModeStatic)
                return;

            DispatcherHelper.Initialize();
#if DEBUG
            _navigationService.NavigateTo("StudentCatalog");
#endif
        }

        public void OnWindowClosing()
        {
        }

        public RelayCommand<string> PageSelectCommand => _pageSelectCommand ?? (_pageSelectCommand =
                                                             new RelayCommand<string>(
                                                                 arg =>
                                                                 {
                                                                     var data = arg.Split('|');
                                                                     _navigationService.NavigateTo(data[0], data.Length > 1 ? data[1] : null);
                                                                 }));

        public RelayCommand ExportAnketCommand => _exportAnketCommand ?? (_exportAnketCommand =
                                                             new RelayCommand(ExportAnket));

        private void ExportAnket()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog()
            {
                AddExtension = true,
                CheckFileExists = false,
                CheckPathExists = true,
                CreatePrompt = false,
                DefaultExt = ".docx",
                Title = "Экспорт анкетных данных",
                ValidateNames = true,
                FileName = $"Анкеты_{DateTime.Now:yyyy}.docx",
                Filter = "Word Document|*.docx",
                OverwritePrompt = true,
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                List<Student> students;
                using (var dh = new DatabaseHelper())
                {
                    var repo = dh.GetRepo<StudentRepository>();
                    students = repo.GetAllStudents();
                }
                var anketDoc = new Anket(students);
                anketDoc.Save(saveFileDialog.FileName);
                Process.Start(saveFileDialog.FileName);
            }

        }
    }
}
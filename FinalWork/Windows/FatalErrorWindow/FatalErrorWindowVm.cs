﻿using System;
using System.Windows;
using FinalWork.UI.Workspaces;
using GalaSoft.MvvmLight.Command;

namespace FinalWork.Windows.FatalErrorWindow
{
    public class FatalErrorWindowVm : LoginSupportedWorkspace
    {
        private RelayCommand _closeCommand;
        
        private string _exceptionMessage;
        private string _callStack;
        private string _app;
        private DateTime _dateTime;
        private string _message;

        public FatalErrorWindowVm()
        {

        }

        public string ExceptionMessage
        {
            get { return _exceptionMessage; }
            set
            {
                _exceptionMessage = value;
                RaisePropertyChanged();
            }
        }

        public string CallStack
        {
            get { return _callStack; }
            set
            {
                _callStack = value;
                RaisePropertyChanged();
            }
        }

        public string App
        {
            get { return _app; }
            set
            {
                _app = value;
                RaisePropertyChanged();
            }
        }

        public DateTime DateTime
        {
            get { return _dateTime; }
            set
            {
                _dateTime = value;
                RaisePropertyChanged();
            }
        }

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                RaisePropertyChanged();
            }
        }

        public RelayCommand CloseCommand => _closeCommand ?? (_closeCommand = new RelayCommand(() => Application.Current.Shutdown()));
    }
}

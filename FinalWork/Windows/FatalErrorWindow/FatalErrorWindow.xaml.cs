﻿using System.Windows;
using FinalWork.UI.Utils.Windows;

namespace FinalWork.Windows.FatalErrorWindow
{
    /// <summary>
    /// Interaction logic for FatalErrorWindow.xaml
    /// </summary>
    public partial class FatalErrorWindow : WindowBase
    {
        public FatalErrorWindow()
        {
            InitializeComponent();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }


}
